﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace RussianCheckers
{
    class AlphaBeta
    {
        long maxDepth = 0;
        long node = 1;
        long maxPrun = 0;
        long minPrun = 0;

        public AlphaBeta()
        {

        }

        public Tuple<int, int, int> alphabeta(Environment env, int depth, Tuple<int, int, int> alpha, Tuple<int, int, int> beta)
        {
            Console.WriteLine("depth = " + depth + "\n" +
                              "node = " + node + "\n" +
                              "maxPruned = " + maxPrun + "\n" +
                              "minPruned = " + minPrun + "\n");

            List<Vector2> player = new List<Vector2>();
            List<Vector2> ai = new List<Vector2>();
            foreach (Entity ent in env.player)
                player.Add(new Vector2(ent.position.X, ent.position.Y));
            foreach (Entity ent in env.ai)
                ai.Add(new Vector2(ent.position.X, ent.position.Y));
            bool playerTurn = env.playerTurn;

            env.selected = null;
            env.resetMarkers();
            env.resetJump();
            env.resetCanMove();

            env.generateCanJump();
            if (env.canMove.Count == 0)
                env.generateCanMove();

            if (env.gameOver() || depth == 0)
                return new Tuple<int, int, int>(env.utility(), 0, 0);

            if (!env.playerTurn)
            {
                for (int i = 0; i < env.canMove.Count; i++)
                {
                    env.selected = env.canMove[i];
                    env.resetCanMove();
                    env.generateJump(env.selected);
                    if (env.markers[0].position == Vector2.Zero)
                        env.generateMove();
                    for (int j = 0; j < env.markers.Count && env.markers[j].position != Vector2.Zero; j++)
                    {
                        node++;
                        env.doJump(j);
                        env.selected.newPosition(env.markers[j].position);
                        env.selected = null;
                        env.resetMarkers();
                        env.resetJump();
                        env.playerTurn = !env.playerTurn;

                        alpha = new Tuple<int, int, int>(Math.Max(alpha.Item1, alphabeta(env, depth - 1, alpha, beta).Item1), i, j);

                        for (int k = 0; k < player.Count; k++)
                        {
                            env.player[k].newPosition(player[k]);
                            env.ai[k].newPosition(ai[k]);
                        }
                        env.playerTurn = playerTurn;

                        env.resetCanMove();
                        env.selected = null;
                        env.resetMarkers();
                        env.resetJump();

                        env.generateCanJump();
                        if (env.canMove.Count == 0)
                            env.generateCanMove();
                        env.selected = env.canMove[i];
                        //env.resetCanMove();
                        env.generateJump(env.selected);
                        if (env.markers[0].position == Vector2.Zero)
                            env.generateMove();

                        if (beta.Item1 <= alpha.Item1)
                        {
                            maxPrun++;
                            break;
                        }
                    }
                    if (beta.Item1 <= alpha.Item1)
                    {
                        maxPrun++;
                        break;
                    }
                }
                return alpha;
            }
            else
            {
                for (int i = 0; i < env.canMove.Count; i++)
                {
                    env.selected = env.canMove[i];
                    env.resetCanMove();
                    env.generateJump(env.selected);
                    if (env.markers[0].position == Vector2.Zero)
                        env.generateMove();
                    for (int j = 0; j < env.markers.Count && env.markers[j].position != Vector2.Zero; j++)
                    {
                        node++;
                        env.doJump(j);
                        env.selected.newPosition(env.markers[j].position);
                        env.selected = null;
                        env.resetMarkers();
                        env.resetJump();
                        env.playerTurn = !env.playerTurn;

                        beta = new Tuple<int, int, int>(Math.Min(beta.Item1, alphabeta(env, depth - 1, alpha, beta).Item1), i, j);

                        for (int k = 0; k < player.Count; k++)
                        {
                            env.player[k].newPosition(player[k]);
                            env.ai[k].newPosition(ai[k]);
                        }
                        env.playerTurn = playerTurn;

                        env.resetCanMove();
                        env.selected = null;
                        env.resetMarkers();
                        env.resetJump();

                        env.generateCanJump();
                        if (env.canMove.Count == 0)
                            env.generateCanMove();
                        env.selected = env.canMove[i];
                        //env.resetCanMove();
                        env.generateJump(env.selected);
                        if (env.markers[0].position == Vector2.Zero)
                            env.generateMove();

                        if (beta.Item1 <= alpha.Item1)
                        {
                            minPrun++;
                            break;
                        }
                    }
                    if (beta.Item1 <= alpha.Item1)
                    {
                        maxPrun++;
                        break;
                    }
                }
                return beta;
            }
        }

        public AlphaBeta(Environment env)
        {
            List<Vector2> player = new List<Vector2>();
            List<Vector2> ai = new List<Vector2>();
            for (int i = 0; i < env.player.Count; i++)
            {
                player.Add(new Vector2(env.player[i].position.X, env.player[i].position.Y));
                ai.Add(new Vector2(env.ai[i].position.X, env.ai[i].position.Y));
            }
            bool playerTurn = env.playerTurn;

            Tuple<int, int, int> value = max(env, -20, 20, 0);

            for (int i = 0; i < player.Count; i++)
            {
                env.player[i].newPosition(player[i]);
                env.ai[i].newPosition(ai[i]);
            }
            env.playerTurn = playerTurn;
            env.resetCanMove();
            env.selected = null;
            env.resetMarkers();
            env.resetJump();

            env.generateCanJump();
            if (env.canMove.Count == 0)
                env.generateCanMove();
            env.selected = env.canMove[value.Item2];
            env.resetCanMove();
            env.generateJump(env.selected);
            if (env.markers[0].position == Vector2.Zero)
                env.generateMove();
            env.doJump(value.Item3);
            env.selected.newPosition(env.markers[value.Item3].position);
            env.selected = null;
            env.resetMarkers();
            env.resetJump();
            env.playerTurn = !env.playerTurn;

            Console.WriteLine("maxDepth = " + maxDepth + "\n" +
                              "node = " + node + "\n" +
                              "maxPruned = " + maxPrun + "\n" +
                              "minPruned = " + minPrun + "\n");
        }

        Tuple<int, int, int> max(Environment env, int alpha, int beta, long depth)
        {
            if (depth > maxDepth)
                maxDepth = depth;
            node++;
            Console.WriteLine("depth = " + depth + "\n" +
                              "node = " + node + "\n" +
                              "maxPruned = " + maxPrun + "\n" +
                              "minPruned = " + minPrun + "\n" +
                              "Alpha beta= " + alpha + ", " + beta + "\n");

            List<Vector2> player = new List<Vector2>();
            List<Vector2> ai = new List<Vector2>();
            for (int i = 0; i < env.player.Count; i++)
            {
                player.Add(new Vector2(env.player[i].position.X, env.player[i].position.Y));
                ai.Add(new Vector2(env.ai[i].position.X, env.ai[i].position.Y));
            }
            bool playerTurn = env.playerTurn;

            env.generateCanJump();
            if (env.canMove.Count == 0)
                env.generateCanMove();

            if (env.gameOver())
                return new Tuple<int, int, int>(env.utility(), 0, 0);
            Tuple<int, int, int> value = new Tuple<int, int, int>(-20, 0, 0);

            for (int i = 0; i < env.canMove.Count; i++)
            {
                env.selected = null;
                env.resetMarkers();
                env.resetJump();
                //env.resetCanMove();

                env.selected = env.canMove[i];
                env.generateJump(env.selected);
                if (env.markers[0].position == Vector2.Zero)
                    env.generateMove();
                for (int j = 0; j < env.markers.Count && env.markers[j].position != Vector2.Zero; j++)
                {
                    env.resetCanMove();
                    env.doJump(j);
                    env.selected.newPosition(env.markers[j].position);
                    env.selected = null;
                    env.resetMarkers();
                    env.resetJump();
                    env.playerTurn = !env.playerTurn;
                   
                    value = new Tuple<int, int, int>(Math.Max(value.Item1, min(env, alpha, beta, depth + 1).Item1), i, j);

                    
                    
                    if (value.Item1 >= beta)
                    {
                        maxPrun++;
                        return value;
                    }
                    alpha = Math.Max(alpha, value.Item1);

                    for (int k = 0; k < player.Count; k++)
                    {
                        env.player[k].newPosition(player[k]);
                        env.ai[k].newPosition(ai[k]);
                    }
                    env.playerTurn = playerTurn;

                    env.resetCanMove();
                    env.selected = null;
                    env.resetMarkers();
                    env.resetJump();

                    env.generateCanJump();
                    if (env.canMove.Count == 0)
                        env.generateCanMove();
                    env.selected = env.canMove[i];
                    env.generateJump(env.selected);
                    if (env.markers[0].position == Vector2.Zero)
                        env.generateMove();
                }
            }
            return value;
        }

        Tuple<int, int, int> min(Environment env, int alpha, int beta, long depth)
        {
            if (depth > maxDepth)
                maxDepth = depth;
            node++;
            Console.WriteLine("depth = " + depth + "\n" +
                              "node = " + node + "\n" +
                              "maxPruned = " + maxPrun + "\n" +
                              "minPruned = " + minPrun + "\n" +
                              "Alpha beta= " + alpha + ", " + beta + "\n");

            List<Vector2> player = new List<Vector2>();
            List<Vector2> ai = new List<Vector2>();
            for (int i = 0; i < env.player.Count; i++)
            {
                player.Add(new Vector2(env.player[i].position.X, env.player[i].position.Y));
                ai.Add(new Vector2(env.ai[i].position.X, env.ai[i].position.Y));
            }
            bool playerTurn = env.playerTurn;

            env.generateCanJump();
            if (env.canMove.Count == 0)
                env.generateCanMove();

            if (env.gameOver())
                return new Tuple<int, int, int>(env.utility(), 0, 0);
            Tuple<int, int, int> value = new Tuple<int, int, int>(20, 0, 0);

            for (int i = 0; i < env.canMove.Count; i++)
            {
                env.selected = null;
                env.resetMarkers();
                env.resetJump();
                //env.resetCanMove();

                env.selected = env.canMove[i];
                env.generateJump(env.selected);
                if (env.markers[0].position == Vector2.Zero)
                    env.generateMove();
                for (int j = 0; j < env.markers.Count && env.markers[j].position != Vector2.Zero; j++)
                {
                    env.resetCanMove();
                    env.doJump(j);
                    env.selected.newPosition(env.markers[j].position);
                    env.selected = null;
                    env.resetMarkers();
                    env.resetJump();
                    env.playerTurn = !env.playerTurn;

                    value = new Tuple<int, int, int>(Math.Min(value.Item1, max(env, alpha, beta, depth + 1).Item1), i, j);

                    
                    
                    if (value.Item1 <= alpha)
                    {
                        minPrun++;
                        return value;
                    }
                    beta = Math.Min(beta, value.Item1);

                    for (int k = 0; k < player.Count; k++)
                    {
                        env.player[k].newPosition(player[k]);
                        env.ai[k].newPosition(ai[k]);
                    }
                    env.playerTurn = playerTurn;

                    env.resetCanMove();
                    env.selected = null;
                    env.resetMarkers();
                    env.resetJump();

                    env.generateCanJump();
                    if (env.canMove.Count == 0)
                        env.generateCanMove();
                    env.selected = env.canMove[i];
                    //env.resetCanMove();
                    env.generateJump(env.selected);
                    if (env.markers[0].position == Vector2.Zero)
                        env.generateMove();
                }
            }
            return value;
        }

        //        void alphabeta(node, depth, α, β, Player)         
        //    if  depth = 0 or node is a terminal node
        //        return the heuristic value of node
        //    if  Player = MaxPlayer
        //        for each child of node
        //            α := max(α, alphabeta(child, depth-1, α, β, not(Player) ))     
        //            if β ≤ α
        //                break                             (* Beta cut-off *)
        //        return α
        //    else
        //        for each child of node
        //            β := min(β, alphabeta(child, depth-1, α, β, not(Player) ))     
        //            if β ≤ α
        //                break                             (* Alpha cut-off *)
        //        return β
        //(* Initial call *)
        //alphabeta(origin, depth, -infinity, +infinity, MaxPlayer)

        
    }
}
