﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace RussianCheckers
{
    class AlphaBeta
    {
        public AlphaBeta()
        {
        }

        public Tuple<int, int> alphabeta(Environment env)
        {
            Tuple<int, int> action = new Tuple<int,int>(-1,-1);
            int value = max(env, -20, 20);
            for(int i = 0; i < env.canMove.Count; i++)
            {
                Environment envirn = new Environment(env);
                envirn.selected = envirn.canMove[i];
                envirn.resetCanMove();
                envirn.generateJump(envirn.selected);
                if (envirn.markers[0].position == Vector2.Zero)
                    envirn.generateMove();
                for (int j = 0; j < envirn.markers.Count; i++)
                {
                    envirn.doJump(i);
                    envirn.selected.newPosition(envirn.markers[i].position);
                    envirn.selected = null;
                    envirn.resetMarkers();
                    envirn.resetJump();
                    envirn.playerTurn = !envirn.playerTurn;
                    if(value == envirn.utility())
                        action = new Tuple<int,int>(i, j);
                }
            }
            return action;
        }

        int max(Environment env, int alpha, int beta)
        {
            if (env.gameOver())
                return env.utility();
            int value = -20;

            for (int i = 0; i < env.canMove.Count; i++)
            {
                Environment envirn = new Environment(env);
                envirn.selected = (Player)envirn.canMove[i];
                envirn.resetCanMove();
                envirn.generateJump(envirn.selected);
                if (envirn.markers[0].position == Vector2.Zero)
                    envirn.generateMove();
                for (int j = 0; j < envirn.markers.Count; j++)
                {
                    Environment child = new Environment(envirn);
                    child.doJump(j);
                    child.selected.newPosition(child.markers[j].position);
                    child.selected = null;
                    child.resetMarkers();
                    child.resetJump();
                    child.playerTurn = !child.playerTurn;

                    child.generateCanJump();
                    if (child.canMove.Count == 0)
                        child.generateCanMove();
                    value = Math.Max(value, min(child, alpha, beta));

                    if (value >= beta)
                        return value;
                    alpha = Math.Max(alpha, value);
                }
            }
            return value;
        }

        int min(Environment env, int alpha, int beta)
        {
            if (env.gameOver())
                return env.utility();
            int value = 20;

            for (int i = 0; i < env.canMove.Count; i++)
            {
                Environment envirn = new Environment(env);
                envirn.selected = envirn.canMove[i];
                envirn.resetCanMove();
                envirn.generateJump(envirn.selected);
                if (envirn.markers[0].position == Vector2.Zero)
                    envirn.generateMove();
                for (int j = 0; j < envirn.markers.Count; j++)
                {
                    Environment child = new Environment(envirn);
                    child.doJump(j);
                    child.selected.newPosition(child.markers[j].position);
                    child.selected = null;
                    child.resetMarkers();
                    child.resetJump();
                    child.playerTurn = !child.playerTurn;

                    child.generateCanJump();
                    if (child.canMove.Count == 0)
                        child.generateCanMove();
                    value = Math.Min(value, max(child, alpha, beta));

                    if (value >= alpha)
                        return value;
                    beta = Math.Min(beta, value);
                }
            }
            return value;
        }
        //        void alphabeta(node, depth, α, β, Player)         
        //    if  depth = 0 or node is a terminal node
        //        return the heuristic value of node
        //    if  Player = MaxPlayer
        //        for each child of node
        //            α := max(α, alphabeta(child, depth-1, α, β, not(Player) ))     
        //            if β ≤ α
        //                break                             (* Beta cut-off *)
        //        return α
        //    else
        //        for each child of node
        //            β := min(β, alphabeta(child, depth-1, α, β, not(Player) ))     
        //            if β ≤ α
        //                break                             (* Alpha cut-off *)
        //        return β
        //(* Initial call *)
        //alphabeta(origin, depth, -infinity, +infinity, MaxPlayer)

        
    }
}
