﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace RussianCheckers
{
    class AlphaBeta
    {
        long maxDepth = 0;
        long node = 1;
        long maxPrun = 0;
        long minPrun = 0;
        DateTime start;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="env">State</param>
        public AlphaBeta(Environment env)
        {
            if (!env.gameOver())
            {
                start = DateTime.Now;
                Tuple<int, int, int> value;
                value = max(env, -100000, 100000, 1);
                long elapsedTicks = DateTime.Now.Ticks - start.Ticks;   //time it took to do alpha beta
                TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);      //change into display able time

                env.doJump(value.Item2, value.Item3); // perfom move returned
                env.possibleMoves[value.Item2].piece.newPosition(env.possibleMoves[value.Item2].markers[value.Item3].position);
                env.reset();    //rest for next player

                env.updateText(1, "Controls\n" +
                                  " >> Movable pieces are\n" +
                                  "    highlighted in BLUE\n" +
                                  " >> Click to select a piece\n" +
                                  " >> Selected piece show\n" +
                                  "    possible moves in YELLOW\n" +
                                  " >> Click to move selected\n" +
                                  "    piece\n" +
                                  "________________________\n" +
                                  "Max depth = " + maxDepth + "\n" +
                                  "Nodes generated = " + node + "\n" +
                                  "Max pruned = " + maxPrun + "\n" +
                                  "Min pruned = " + minPrun + "\n" +
                                  "Value = " + value.Item1 + "\n" +
                                  "Time = " + elapsedSpan.TotalSeconds + " sec");
            }
            else
                env.winner = true;
        }

        /// <summary>
        /// Max Value
        /// </summary>
        /// <param name="env">State</param>
        /// <param name="alpha">Alpha value</param>
        /// <param name="beta">Beta value</param>
        /// <param name="depth">Depth of current node</param>
        /// <returns>Value with corrisponding move</returns>
        Tuple<int, int, int> max(Environment env, int alpha, int beta, long depth)
        {
            if (depth > maxDepth)
                maxDepth = depth;
            node++;
            //if (node % 100000 == 0)
            //    Console.WriteLine("depth = " + depth + "\n" +
            //                  "node = " + node + "\n" +
            //                  "maxPruned = " + maxPrun + "\n" +
            //                  "minPruned = " + minPrun + "\n" +
            //                  "Alpha beta= " + alpha + ", " + beta + "\n");

            //save pieces posisitons to reset to for next move in list of possible moves
            List<Vector2> player = new List<Vector2>();
            List<Vector2> ai = new List<Vector2>();
            for (int i = 0; i < env.player.Count; i++)
            {
                player.Add(new Vector2(env.player[i].position.X, env.player[i].position.Y));
                ai.Add(new Vector2(env.ai[i].position.X, env.ai[i].position.Y));
            }
            //terminal state or hit cutoff
            if (env.gameOver() || depth == 16 + env.turn /*Math.Pow(2.0, (double)env.turn)*/)
            {
                switch (env.aiLevel)
                {
                    case 1:
                        return new Tuple<int, int, int>(env.utility1(), 0, 0);      //AI Level 1
                    case 2:
                        return new Tuple<int, int, int>(env.utility2(), 0, 0);      //AI Level 2
                    case 3:
                        return new Tuple<int, int, int>(env.utility3(), 0, 0);      //AI Level 3
                    case 4:
                        return new Tuple<int, int, int>(env.utility4(), 0, 0);      //AI Level 4
                }
            }
            Tuple<int, int, int> value = new Tuple<int, int, int>(-100000, 0, 0);
            for (int i = 0; i < env.possibleMoves.Count; i++)
            {
                for (int j = 0; j < env.possibleMoves[i].markers.Count && env.possibleMoves[i].markers[j].position != Vector2.Zero; j++)
                {
                    if (env.possibleMoves[i].jumpSet[j].Count >= 3)                                     //if there is a jump to take 3 or more pieces take it
                        return new Tuple<int, int, int>(100000, i, j);
                    else if (depth == 1 && env.possibleMoves.Count == 1 &&
                             env.possibleMoves[i].markers[j + 1].position == Vector2.Zero)              //if there is only 1 move take it
                        return new Tuple<int, int, int>(0, i, j);
                    env.doJump(i, j);                                                                   //dojump with piece i and jump j for that piece
                    env.possibleMoves[i].piece.newPosition(env.possibleMoves[i].markers[j].position);   //move the piece to its final position if it were to move there
                    env.reset();                                                                        //reset for next player
                    env.generateCanJump();                                                              //generate list of pieces that can move
                    env.generateJump();                                                                 //generate move for the list of pieces that can move
                    Tuple<int, int, int> minimum = min(env, alpha, beta, depth + 1);
                    int score = Math.Max(value.Item1, minimum.Item1);
                    if(value.Item1 != score)
                        value = new Tuple<int, int, int>(score, i, j);                                  //returned value is better, us that move for next iteration
                    for (int k = 0; k < player.Count; k++)                                              //reset pieces to previous configuration to perform next move/action
                    {
                        env.player[k].newPosition(player[k]);
                        env.ai[k].newPosition(ai[k]);
                    }
                    env.reset();                                                                        //reset for next player(current player since it was reset before in the min)
                    env.generateCanJump();
                    env.generateJump();
                    if (value.Item1 >= beta)                                                            //prunning check
                    {
                        maxPrun++;
                        return value;
                    }
                    alpha = Math.Max(alpha, value.Item1);
                }
            }
            return value;
        }

        /// <summary>
        /// Min Value
        /// </summary>
        /// <param name="env">State</param>
        /// <param name="alpha">Alpha value</param>
        /// <param name="beta">Beta value</param>
        /// <param name="depth">Depth of current node</param>
        /// <returns>Value with corrisponding move</returns>
        Tuple<int, int, int> min(Environment env, int alpha, int beta, long depth)
        {
            if (depth > maxDepth)
                maxDepth = depth;
            node++;
            //if (node % 100000 == 0)
            //Console.WriteLine("depth = " + depth + "\n" +
            //                  "node = " + node + "\n" +
            //                  "maxPruned = " + maxPrun + "\n" +
            //                  "minPruned = " + minPrun + "\n" +
            //                  "Alpha beta= " + alpha + ", " + beta + "\n");
            //save pieces posisitons to reset to for next move in list of possible moves
            List<Vector2> player = new List<Vector2>();
            List<Vector2> ai = new List<Vector2>();
            for (int i = 0; i < env.player.Count; i++)
            {
                player.Add(new Vector2(env.player[i].position.X, env.player[i].position.Y));
                ai.Add(new Vector2(env.ai[i].position.X, env.ai[i].position.Y));
            }
            //terminal state or hit cutoff
            if (env.gameOver() || depth == 16 + env.turn/*Math.Pow(2.0, (double)env.turn)*/)
            {
                switch(env.aiLevel)
                {
                    case 1:
                        return new Tuple<int, int, int>(env.utility1(), 0, 0);      //AI Level 1
                    case 2:
                        return new Tuple<int, int, int>(env.utility2(), 0, 0);      //AI Level 2
                    case 3:
                        return new Tuple<int, int, int>(env.utility3(), 0, 0);      //AI Level 3
                    case 4:
                        return new Tuple<int, int, int>(env.utility4(), 0, 0);      //AI Level 4
                }
            }
            Tuple<int, int, int> value = new Tuple<int, int, int>(100000, 0, 0);
            for (int i = 0; i < env.possibleMoves.Count; i++)
            {
                for (int j = 0; j < env.possibleMoves[i].markers.Count && env.possibleMoves[i].markers[j].position != Vector2.Zero; j++)
                {
                    if (env.possibleMoves[i].jumpSet[j].Count >= 3)                                     //if there is a jump to take 3 or more pieces take it
                        return new Tuple<int, int, int>(-100000, i, j);
                    else if (depth == 1 && env.possibleMoves.Count == 1 && 
                             env.possibleMoves[i].markers[j + 1].position == Vector2.Zero)              //if there is only 1 move take it
                        return new Tuple<int, int, int>(0, i, j);
                    env.doJump(i, j);                                                                   //dojump with piece i and jump j for that piece
                    env.possibleMoves[i].piece.newPosition(env.possibleMoves[i].markers[j].position);   //move the piece to its final position if it were to move there
                    env.reset();                                                                        //reset for next player
                    env.generateCanJump();                                                              //generate list of pieces that can move
                    env.generateJump();                                                                 //generate move for the list of pieces that can move
                    Tuple<int, int, int> maximum = max(env, alpha, beta, depth + 1);
                    int score = Math.Min(value.Item1, maximum.Item1);
                    if(value.Item1 != score)
                        value = new Tuple<int, int, int>(score, i, j);                                  //returned value is better, us that move for next iteration

                    for (int k = 0; k < player.Count; k++)                                              //reset pieces to previous configuration to perform next move/action
                    {
                        env.player[k].newPosition(player[k]);
                        env.ai[k].newPosition(ai[k]);
                    }
                    env.reset();                                                                        //reset for next player(current player since it was reset before in the min)
                    env.generateCanJump();
                    env.generateJump();
                    if (value.Item1 <= alpha)                                                           //prunning check
                    {
                        minPrun++;
                        return value;
                    }
                    beta = Math.Min(beta, value.Item1);
                }
            }
            return value;
        }
    }
}
