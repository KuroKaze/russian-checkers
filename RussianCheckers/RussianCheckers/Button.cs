﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

/// <summary>
/// Improvements:
/// button clicked animation
/// </summary>

class Button
{
    Texture2D spriteSheet;
    Vector2 position;
    Input mouse;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="texture">Button texture</param>
    /// <param name="loc">Button location</param>
    /// <param name="mos">Mouse handler</param>
    public Button(Texture2D texture, Vector2 loc, Input mos)
    {
        spriteSheet = texture;
        position = loc;
        mouse = mos;
    }

    /// <summary>
    /// Button clicked
    /// </summary>
    /// <returns>True if button was clicked, false otherwise</returns>
    public bool clicked()
    {
        return mouse.lMouseButtonClicked() && new Rectangle((int)position.X, (int)position.Y, spriteSheet.Width, spriteSheet.Height).Contains((int)mouse.mousePos().X, (int)mouse.mousePos().Y);
    }

    /// <summary>
    /// Change position of button
    /// </summary>
    /// <param name="loc">New location</param>
    public void newPosition(Vector2 loc)
    {
        position = loc;
    }

    /// <summary>
    /// Draw button
    /// </summary>
    /// <param name="spriteBatch">Enables button to be drawn</param>
    public void Draw(SpriteBatch spriteBatch)
    {
        spriteBatch.Draw(spriteSheet, position, Color.White);
    }

}
