﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RussianCheckers
{
    class Collision
    {

        protected List<Entity> allEntities;
        protected Rectangle worldBoundary, screenBoundary;
        public Rectangle cameraBoundary;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="world">Entire game boundry</param>
        /// <param name="screen">Screen boundry</param>
        /// <param name="camera">Camera boundry</param>
        public Collision(Rectangle world, Rectangle screen, Rectangle camera)
        {
            allEntities = new List<Entity>();
            screenBoundary = screen;
            worldBoundary = world;
            cameraBoundary = camera;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="collision">Collition to b copied</param>
        public Collision(Collision collision)
        {
            allEntities = new List<Entity>();
            for (int i = 0; i < collision.allEntities.Count; i++)
            {
                if(i < 12)
                    allEntities.Add(new Player((Player)collision.allEntities[i]));
                else
                    allEntities.Add(new Entity(collision.allEntities[i]));
            }
            worldBoundary = collision.worldBoundary;
            screenBoundary = collision.screenBoundary;
            cameraBoundary = collision.cameraBoundary;
        }

        /// <summary>
        /// Add entity to list to do collision detection
        /// </summary>
        /// <param name="ent">Entity tobe added</param>
        public virtual void add(Entity ent)
        {
            allEntities.Add(ent);
        }

        /// <summary>
        /// Add a list of entities
        /// </summary>
        /// <param name="listEnt">List of entities tobe added</param>
        public virtual void add(List<Entity> listEnt)
        {
            allEntities.AddRange(listEnt);
        }

        /// <summary>
        /// Remove entity from list (it can't collide with anything anymore)
        /// </summary>
        /// <param name="ent">Entity tobe removed</param>
        public virtual void remove(Entity ent)
        {
            allEntities.Remove(ent);
        }

        /// <summary>
        /// Remove entity from list
        /// </summary>
        /// <param name="idx">Index of entity tobe removed</param>
        public virtual void remove(int idx)
        {
            allEntities.RemoveAt(idx);
        }

        public virtual int size()
        {
            return allEntities.Count;
        }

        /// <summary>
        /// Check of entity has moved off screen
        /// </summary>
        /// <param name="obj">Entity you want to check</param>
        /// <returns>True if off screen</returns>
        public virtual bool moveOffWorld(Entity obj)
        {
            return hitLeftWorld(obj) || hitTopWorld(obj) || hitRightWorld(obj) || hitBottomWorld(obj);
        }

        public virtual bool moveOffWorld(Rectangle rec)
        {
            return hitLeftWorld(rec) || hitTopWorld(rec) || hitRightWorld(rec) || hitBottomWorld(rec);
        }

        public virtual bool hitRightWorld(Entity obj)
        {
            return obj.boundingBox.Right > worldBoundary.Right;
        }

        public virtual bool hitRightWorld(Rectangle rec)
        {
            return rec.Right > worldBoundary.Right;
        }

        public virtual bool hitLeftWorld(Entity obj)
        {
            return obj.boundingBox.Left < worldBoundary.Left;
        }

        public virtual bool hitLeftWorld(Rectangle rec)
        {
            return rec.Left < worldBoundary.Left;
        }

        public virtual bool hitTopWorld(Entity obj)
        {
            return obj.boundingBox.Top < worldBoundary.Top;
        }

        public virtual bool hitTopWorld(Rectangle rec)
        {
            return rec.Top < worldBoundary.Top;
        }

        public virtual bool hitBottomWorld(Entity obj)
        {
            return obj.boundingBox.Bottom > worldBoundary.Bottom;
        }

        public virtual bool hitBottomWorld(Rectangle rec)
        {
            return rec.Bottom > worldBoundary.Bottom;
        }

        public virtual bool moveOffScreen(Rectangle rect)
        {
            return hitLeftScreen(rect) || hitTopScreen(rect) || hitRightScreen(rect) || hitBottomScreen(rect);
        }

        public virtual bool hitRightScreen(Rectangle rect)
        {
            return rect.Right > screenBoundary.Right;
        }

        public virtual bool hitLeftScreen(Rectangle rect)
        {
            return rect.Left < screenBoundary.Left;
        }

        public virtual bool hitTopScreen(Rectangle rect)
        {
            return rect.Top < screenBoundary.Top;
        }

        public virtual bool hitBottomScreen(Rectangle rect)
        {
            return rect.Bottom > screenBoundary.Bottom;
        }

        public virtual bool moveOffCamera(Entity obj)
        {
            return hitLeftCamera(obj) || hitTopCamera(obj) || hitRightCamera(obj) || hitBottomCamera(obj);
        }

        public virtual bool hitRightCamera(Entity obj)
        {
            return obj.boundingBox.Right > cameraBoundary.Right;
        }

        public virtual bool hitLeftCamera(Entity obj)
        {
            return obj.boundingBox.Left < cameraBoundary.Left;
        }

        public virtual bool hitTopCamera(Entity obj)
        {
            return obj.boundingBox.Top < cameraBoundary.Top;
        }

        public virtual bool hitBottomCamera(Entity obj)
        {
            return obj.boundingBox.Bottom > cameraBoundary.Bottom;
        }

        /// <summary>
        /// Check if entity is colliding with another entity
        /// </summary>
        /// <param name="obj">Entity being checked</param>
        /// <returns>Entity that collided with entity that was passed in</returns>
        public virtual Entity collide(Entity obj)
        {
            int idx = allEntities.FindIndex(
                delegate(Entity ent)
                {
                    return ent.position == obj.position;
                });
            for (int i = 0; i < allEntities.Count; i++)
            {
                if (i != idx)
                {
                    if (obj.boundingBox.Intersects(allEntities[i].boundingBox))
                        return intersectPixels(obj, allEntities[i]) ? allEntities[i] : null;
                }
            }
            return null;
        }

        /// <summary>
        /// Pixel collision detection
        /// </summary>
        /// <param name="obj1">Entity 1</param>
        /// <param name="obj2">Entity 2</param>
        /// <returns>True if entity 1 is colliding with entity 2, false otherwise</returns>
        protected static bool intersectPixels(Entity obj1, Entity obj2)
        {
            // Calculate a matrix which transforms from A's local space into
            // world space and then into B's local space
            Matrix transformAToB = obj1.transformM * Matrix.Invert(obj2.transformM);

            // When a point moves in A's local space, it moves in B's local space with a
            // fixed direction and distance proportional to the movement in A.
            // This algorithm steps through A one pixel at a time along A's X and Y axes
            // Calculate the analogous steps in B:
            Vector2 stepX = Vector2.TransformNormal(Vector2.UnitX, transformAToB);
            Vector2 stepY = Vector2.TransformNormal(Vector2.UnitY, transformAToB);

            // Calculate the top left corner of A in B's local space
            // This variable will be reused to keep track of the start of each row
            Vector2 yPosInB = Vector2.Transform(Vector2.Zero, transformAToB);

            // For each row of pixels in A
            for (int yA = 0; yA < obj1.srcRect.Height; yA++)
            {
                // Start at the beginning of the row
                Vector2 posInB = yPosInB;

                // For each pixel in this row
                for (int xA = 0; xA < obj1.srcRect.Width; xA++)
                {
                    // Round to the nearest pixel
                    int xB = (int)Math.Round(posInB.X);
                    int yB = (int)Math.Round(posInB.Y);

                    // If the pixel lies within the bounds of B
                    if (0 <= xB && xB < obj2.srcRect.Width &&
                        0 <= yB && yB < obj2.srcRect.Height)
                    {
                        // Get the colors of the overlapping pixels
                        Color colorA = obj1.textureData[xA + yA * obj1.srcRect.Width];
                        Color colorB = obj2.textureData[xB + yB * obj2.srcRect.Width];

                        // If both pixels are not completely transparent,
                        if (colorA.A != 0 && colorB.A != 0)
                        {
                            // then an intersection has been found
                            return true;
                        }
                    }

                    // Move to the next pixel in the row
                    posInB += stepX;
                }

                // Move to the next row
                yPosInB += stepY;
            }

            // No intersection found
            return false;
        }

        /// <summary>
        /// Check if space is ocuppied by an entity/entities
        /// </summary>
        /// <param name="rect">Rectangle space being checked</param>
        /// <returns>Enities that is ocupping that space</returns>
        public List<Entity> collide(Rectangle rect)
        {
            List<Entity> selected = new List<Entity>();
            for (int i = 0; i < allEntities.Count; i++)
            {
                if (allEntities[i].boundingBox.Intersects(rect))
                    selected.Add(allEntities[i]);
            }
            return selected;
        }

        /// <summary>
        /// Check if space is ocuppied by an entity/entities
        /// </summary>
        /// <param name="circle">Circle space being checked</param>
        /// <returns>Enities that is ocupping that space</returns>
        public List<Entity> collide(Circle circle)
        {
            List<Entity> selected = new List<Entity>();
            for (int i = 0; i < allEntities.Count; i++)
            {
                if (circle.Intersects(allEntities[i].boundingBox))
                    selected.Add(allEntities[i]);
            }
            return selected;
        }

        /// <summary>
        /// Check the closest entity ocuppying space
        /// </summary>
        /// <param name="rect">Rectangle space being checked</param>
        /// <returns>Closest entity occupying space, null if no entity ocuppies it</returns>
        public Entity collideClosest(Rectangle rect)
        {
            Entity closest = null;
            List<Entity> selected = collide(rect);
            if (selected.Count == 0)
                return closest;
            else if (selected.Count == 1)
                return selected[0];
            else
            {
                float closestDistance = float.MaxValue;
                foreach (Entity ent in selected)
                {
                    float distance = Vector2.Distance(ent.position, new Vector2(rect.Center.X, rect.Center.Y));
                    if (closest == null || distance < closestDistance)
                    {
                        closest = ent;
                        closestDistance = distance;
                    }
                }
            }
            return closest;
        }
    }
}