﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace RussianCheckers
{
    class Environment : Interface
    {
        public List<Entity> player;
        public List<Entity> ai;
        public List<Entity> canMove;
        public bool playerTurn;
        public Entity selected;
        public List<Entity> markers;
        public List<List<int>> jumpSet;
        bool playerWin;

        //AlphaBeta abs;

        public Environment(Texture2D environment, Rectangle environmentBox, Input inp, Camera cam, Collision colli, Vector2 loc)
            : base(environment, environmentBox, inp, cam, colli, loc)
        {
            player = new List<Entity>();
            ai = new List<Entity>();
            canMove = new List<Entity>();
            markers = new List<Entity>();
            playerTurn = true;
            selected = null;
            jumpSet = new List<List<int>>();
            for (int i = 0; i < 4; i++)
                jumpSet.Add(new List<int>());
            //abs = new AlphaBeta();
        }

        public Environment(Environment env)
            : base(env)
        {
            player = new List<Entity>();
            player = env.player;
            ai = new List<Entity>();
            ai = env.ai;
            canMove = new List<Entity>();
            foreach (Player ent in env.canMove)
                canMove.Add(new Player(ent));
            playerTurn = env.playerTurn;
            selected = env.selected;
            markers = new List<Entity>();
            foreach (Entity ent in env.markers)
                markers.Add(new Entity(ent));
            jumpSet = new List<List<int>>();
            for (int i = 0; i < 4; i++)
                jumpSet.Add(new List<int>());
        }
        
        public void LoadContent(ContentManager Content)
        {
            for (int i = 0; i < 12; i++)
            {
                if (i < 6)
                {
                    if (i < 3)
                        ai.Add(new Player(Content.Load<Texture2D>("Player1"), 1, 2, 0, 2, new Vector2(positions[0].X + 225.0f / 2.0f + (i * 150) - 75.0f, positions[0].Y + 75.0f / 2.0f + 225.0f), true));
                        //ai.Add(new Player(Content.Load<Texture2D>("Player1"), 1, 2, 0, 2, new Vector2(positions[0].X + 225.0f / 2.0f + (i * 150), positions[0].Y + 75.0f / 2.0f), true));
                    else
                        ai.Add(new Player(Content.Load<Texture2D>("Player1"), 1, 2, 0, 2, new Vector2(positions[0].X + 75.0f / 2.0f + ((i - 3) * 150), positions[0].Y + 225.0f / 2.0f), true));
                }
                else
                {
                    if (i < 9)
                        player.Add(new Player(Content.Load<Texture2D>("Player2"), 1, 2, 0, 2, new Vector2(positions[0].X + 225.0f / 2.0f + ((i - 6) * 150), positions[0].Y + 675.0f / 2.0f), false));
                    else
                        player.Add(new Player(Content.Load<Texture2D>("Player2"), 1, 2, 0, 2, new Vector2(positions[0].X + 75.0f / 2.0f + ((i - 9) * 150), positions[0].Y + 825.0f / 2.0f), false));
                }
            }
            for (int i = 0; i < 4; i++)
            {
                markers.Add(new Entity(Content.Load<Texture2D>("PossibleMove"), 1, 1, 0, 0, Vector2.Zero));
            }
            collision.add(player);
            collision.add(ai);
        }

        public override void Update()
        {
            base.Update();
            
            if (!playerTurn && input.keyPressed(Keys.Enter))
            {
                AlphaBeta alphabeta = new AlphaBeta(this);
                //AlphaBeta alphabeta = new AlphaBeta();
                //Tuple<int, int, int> move = alphabeta.alphabeta(this, 100, new Tuple<int, int, int>(-20, 0, 0), new Tuple<int, int, int>(20, 0, 0));
                
                //generateCanJump();
                //if (canMove.Count == 0)
                //    generateCanMove();
                //selected = canMove[0];
                //resetCanMove();
                //selected.animate();
                //generateJump(selected);
                //if (markers[0].position == Vector2.Zero)
                //    generateMove();
                //doJump(0);
                //selected.newPosition(markers[0].position);
                //selected.animate();
                //selected = null;
                //resetMarkers();
                //resetJump();
                //playerTurn = !playerTurn;
                
            }
            else if (input.keyPressed(Keys.Enter))
            {
                AlphaBeta alphabeta = new AlphaBeta(this);
                //if (selected == null && canMove.Count == 0)
                //{
                //    generateCanJump();
                //    if (canMove.Count == 0)
                //        generateCanMove();

                //}
                //foreach (Entity ent in canMove)
                //{
                //    if (input.lMouseBottonClickedOn(ent))
                //    {
                //        selected = ent;
                //        resetCanMove();
                //        selected.animate();
                //        break;
                //    }
                //}
                //if (selected != null)
                //{
                //    generateJump(selected);
                //    if (markers[0].position == Vector2.Zero)
                //        generateMove();
                //}
                //for (int i = 0; i < markers.Count && markers[i].position != Vector2.Zero; i++)
                //{
                //    if (input.lMouseBottonClickedOn(markers[i]))
                //    {
                //        doJump(i);
                //        selected.newPosition(markers[i].position);
                //        selected.animate();
                //        selected = null;
                //        resetMarkers();
                //        resetJump();
                //        playerTurn = !playerTurn;
                //    }
                //}
            }
        }

        public void resetForNextTurn()
        {
            resetCanMove();
            resetJump();
            resetMarkers();
            playerTurn = !playerTurn;
        }

        public void resetCanMove()
        {
            foreach (Entity ent in canMove)
                ent.animate();
            canMove.Clear();
        }

        public void resetMarkers()
        {
            while (collision.size() > 12)
                collision.remove(collision.size() - 1);

            foreach (Entity ent in markers)
                ent.newPosition(Vector2.Zero);
        }

        public void resetJump()
        {
            foreach (List<int> jump in jumpSet)
                jump.Clear();
        }

        public void doJump(int jumpIdx)
        {
            Entity temp = new Entity(selected);
            foreach (int jump in jumpSet[jumpIdx])
            {
                Rectangle topLeft = new Rectangle((int)(temp.boundingBox.X - 75.0f), (int)(temp.boundingBox.Y - 75.0f), 75, 75);
                Rectangle topRight = new Rectangle((int)(temp.boundingBox.X + 75.0f), (int)(temp.boundingBox.Y - 75.0f), 75, 75);
                Rectangle bottomLeft = new Rectangle((int)(temp.boundingBox.X - 75.0f), (int)(temp.boundingBox.Y + 75.0f), 75, 75);
                Rectangle bottomRight = new Rectangle((int)(temp.boundingBox.X + 75.0f), (int)(temp.boundingBox.Y + 75.0f), 75, 75);
                Rectangle oTopLeft = new Rectangle((int)(temp.boundingBox.X - 150.0f), (int)(temp.boundingBox.Y - 150.0f), 75, 75);
                Rectangle oTopRight = new Rectangle((int)(temp.boundingBox.X + 150.0f), (int)(temp.boundingBox.Y - 150.0f), 75, 75);
                Rectangle oBottomLeft = new Rectangle((int)(temp.boundingBox.X - 150.0f), (int)(temp.boundingBox.Y + 150.0f), 75, 75);
                Rectangle oBottomRight = new Rectangle((int)(temp.boundingBox.X + 150.0f), (int)(temp.boundingBox.Y + 150.0f), 75, 75);
                switch (jump)
                {
                    case 1:
                        collision.collide(topLeft)[0].newPosition(Vector2.Zero);
                        temp.newPosition(new Vector2(oTopLeft.Center.X, oTopLeft.Center.Y));
                        break;
                    case 2:
                        collision.collide(topRight)[0].newPosition(Vector2.Zero);
                        temp.newPosition(new Vector2(oTopRight.Center.X, oTopRight.Center.Y));
                        break;
                    case 3:
                        collision.collide(bottomLeft)[0].newPosition(Vector2.Zero);
                        temp.newPosition(new Vector2(oBottomLeft.Center.X, oBottomLeft.Center.Y));
                        break;
                    case 4:
                        collision.collide(bottomRight)[0].newPosition(Vector2.Zero);
                        temp.newPosition(new Vector2(oBottomRight.Center.X, oBottomRight.Center.Y));
                        break;
                }
            }
        }

        public void generateMove()
        {
            int markerIndex = 0;
            if (playerTurn)
            {

                Rectangle topLeft = new Rectangle((int)(selected.boundingBox.X - 75.0f), (int)(selected.boundingBox.Y - 75.0f), 75, 75);
                Rectangle topRight = new Rectangle((int)(selected.boundingBox.X + 75.0f), (int)(selected.boundingBox.Y - 75.0f), 75, 75);

                if (!collision.moveOffWorld(topLeft) && collision.collide(topLeft).Count == 0)
                    markers[markerIndex++].newPosition(new Vector2(topLeft.Center.X, topLeft.Center.Y));
                if (!collision.moveOffWorld(topRight) && collision.collide(topRight).Count == 0)
                    markers[markerIndex++].newPosition(new Vector2(topRight.Center.X, topRight.Center.Y));
            }
            else
            {

                Rectangle bottomLeft = new Rectangle((int)(selected.boundingBox.X - 75.0f), (int)(selected.boundingBox.Y + 75.0f), 75, 75);
                Rectangle bottomRight = new Rectangle((int)(selected.boundingBox.X + 75.0f), (int)(selected.boundingBox.Y + 75.0f), 75, 75);

                if (!collision.moveOffWorld(bottomLeft) && collision.collide(bottomLeft).Count == 0)
                    markers[markerIndex++].newPosition(new Vector2(bottomLeft.Center.X, bottomLeft.Center.Y));
                if (!collision.moveOffWorld(bottomRight) && collision.collide(bottomRight).Count == 0)
                    markers[markerIndex++].newPosition(new Vector2(bottomRight.Center.X, bottomRight.Center.Y));
            }
        }

        public void generateJump(Entity ent)
        {
            for (int i = 0; i < 2; i++ )
            {
                Rectangle topLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                Rectangle topRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                Rectangle bottomLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                Rectangle bottomRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                Rectangle oTopLeft = new Rectangle((int)(ent.boundingBox.X - 150.0f), (int)(ent.boundingBox.Y - 150.0f), 75, 75);
                Rectangle oTopRight = new Rectangle((int)(ent.boundingBox.X + 150.0f), (int)(ent.boundingBox.Y - 150.0f), 75, 75);
                Rectangle oBottomLeft = new Rectangle((int)(ent.boundingBox.X - 150.0f), (int)(ent.boundingBox.Y + 150.0f), 75, 75);
                Rectangle oBottomRight = new Rectangle((int)(ent.boundingBox.X + 150.0f), (int)(ent.boundingBox.Y + 150.0f), 75, 75);

                bool jumpTopLeft = !collision.moveOffWorld(oTopLeft) && collision.collide(oTopLeft).Count == 0 && collision.collide(topLeft).Count > 0 && ((Player)collision.collide(topLeft)[0]).enemy != ((Player)selected).enemy;
                bool jumpTopRight = !collision.moveOffWorld(oTopRight) && collision.collide(oTopRight).Count == 0 && collision.collide(topRight).Count > 0 && ((Player)collision.collide(topRight)[0]).enemy != ((Player)selected).enemy;
                bool jumpBottomLeft = !collision.moveOffWorld(oBottomLeft) && collision.collide(oBottomLeft).Count == 0 && collision.collide(bottomLeft).Count > 0 && ((Player)collision.collide(bottomLeft)[0]).enemy != ((Player)selected).enemy;
                bool jumpBottomRight = !collision.moveOffWorld(oBottomRight) && collision.collide(oBottomRight).Count == 0 && collision.collide(bottomRight).Count > 0 && ((Player)collision.collide(bottomRight)[0]).enemy != ((Player)selected).enemy;

                int result;
                result = markers.FindIndex(
                    delegate(Entity marker)
                    {
                        return marker.position == ent.position;
                    });
                if(result == -1)
                    result = markers.FindIndex(
                    delegate(Entity marker)
                    {
                        return marker.position == Vector2.Zero;
                    });

                if (jumpTopLeft)
                {
                    if (result != -1)
                    {
                        if (i == 1 && jumpSet[result].Count == 0 && result > 0)
                            jumpSet[result].Add(jumpSet[result - 1][0]);
                        jumpSet[result].Add(1);
                        markers[result].newPosition(new Vector2(oTopLeft.Center.X, oTopLeft.Center.Y));
                        collision.add(new Entity(markers[result]));
                        ent = markers[result];
                        result = markers.FindIndex(
                            delegate(Entity marker)
                            {
                                return marker.position == Vector2.Zero;
                            });
                    }
                }
                if (jumpTopRight)
                {
                    if (result != -1)
                    {
                        if (i == 1 && jumpSet[result].Count == 0 && result > 0)
                            jumpSet[result].Add(jumpSet[result - 1][0]);
                        jumpSet[result].Add(2);
                        markers[result].newPosition(new Vector2(oTopRight.Center.X, oTopRight.Center.Y));
                        collision.add(new Entity(markers[result]));
                        ent = markers[result];
                        result = markers.FindIndex(
                            delegate(Entity marker)
                            {
                                return marker.position == Vector2.Zero;
                            });
                    }
                }
                if (jumpBottomLeft)
                {
                    if (result != -1)
                    {
                        if (i == 1 && jumpSet[result].Count == 0 && result > 0)
                            jumpSet[result].Add(jumpSet[result - 1][0]);
                        jumpSet[result].Add(3);
                        markers[result].newPosition(new Vector2(oBottomLeft.Center.X, oBottomLeft.Center.Y));
                        collision.add(new Entity(markers[result]));
                        ent = markers[result];
                        result = markers.FindIndex(
                            delegate(Entity marker)
                            {
                                return marker.position == Vector2.Zero;
                            });
                    }
                }
                if (jumpBottomRight)
                {
                    if (result != -1)
                    {
                        if (i == 1 && jumpSet[result].Count == 0 && result > 0 )
                            jumpSet[result].Add(jumpSet[result - 1][0]);
                        jumpSet[result].Add(4);
                        markers[result].newPosition(new Vector2(oBottomRight.Center.X, oBottomRight.Center.Y));
                        collision.add(new Entity(markers[result]));
                        ent = markers[result];
                        result = markers.FindIndex(
                            delegate(Entity marker)
                            {
                                return marker.position == Vector2.Zero;
                            });
                    }
                }
            }
            int count = 0;
            foreach (List<int> jump in jumpSet)
                count += jump.Count;

            if (count == 3 && jumpSet[0][jumpSet[0].Count - 1] == jumpSet[1][jumpSet[1].Count - 1])
            {
                bool circular = false;
                switch (jumpSet[1][0])
                {
                    case 1:
                        circular = collision.collide(new Rectangle(markers[0].boundingBox.X - 75, markers[0].boundingBox.Y - 75, 75, 75)).Count > 0;
                        break;
                    case 2:
                        circular = collision.collide(new Rectangle(markers[0].boundingBox.X + 75, markers[0].boundingBox.Y - 75, 75, 75)).Count > 0;
                        break;
                    case 3:
                        circular = collision.collide(new Rectangle(markers[0].boundingBox.X - 75, markers[0].boundingBox.Y + 75, 75, 75)).Count > 0;
                        break;
                    case 4:
                        circular = collision.collide(new Rectangle(markers[0].boundingBox.X + 75, markers[0].boundingBox.Y + 75, 75, 75)).Count > 0;
                        break;
                }
                if (circular)
                {
                    jumpSet[0].Add(jumpSet[1][0]);
                    switch (jumpSet[1][1])
                    {
                        case 1:
                            jumpSet[0].Add(4);
                            break;
                        case 2:
                            jumpSet[0].Add(3);
                            break;
                        case 3:
                            jumpSet[0].Add(2);
                            break;
                        case 4:
                            jumpSet[0].Add(1);
                            break;
                    }
                    int i = 0;
                    foreach (int j in jumpSet[0])
                        i += j;
                    jumpSet[0].Add(10 - i);

                    jumpSet[1] = new List<int>();
                    markers[1].newPosition(Vector2.Zero);
                    markers[0].newPosition(selected.position);
                }
            }
        }

        public void generateCanMove()
        {
            if (playerTurn)
            {
                foreach (Entity ent in player)
                {
                    if (ent.position != Vector2.Zero)
                    {
                        Rectangle topLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                        Rectangle topRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);

                        if (!collision.moveOffWorld(topLeft) && collision.collide(topLeft).Count == 0)
                        {
                            canMove.Add(ent);
                            ent.animate();
                        }
                        else if (!collision.moveOffWorld(topRight) && collision.collide(topRight).Count == 0)
                        {
                            canMove.Add(ent);
                            ent.animate();
                        }
                    }
                }
            }
            else
            {
                foreach (Entity ent in ai)
                {
                    if (ent.position != Vector2.Zero)
                    {
                        Rectangle bottomLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                        Rectangle bottomRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);

                        if (!collision.moveOffWorld(bottomLeft) && collision.collide(bottomLeft).Count == 0)
                        {
                            canMove.Add(ent);
                            ent.animate();
                        }
                        else if (!collision.moveOffWorld(bottomRight) && collision.collide(bottomRight).Count == 0)
                        {
                            canMove.Add(ent);
                            ent.animate();
                        }
                    }
                }
            }
        }

        public void generateCanJump()
        {

            List<Entity> temp = new List<Entity>();
            
            if (playerTurn)
                temp = player;
            else
                temp = ai;

            foreach (Entity ent in temp)
            {
                if (ent.position != Vector2.Zero)
                {
                    Rectangle topLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                    Rectangle topRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                    Rectangle bottomLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                    Rectangle bottomRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                    Rectangle oTopLeft = new Rectangle((int)(ent.boundingBox.X - 150.0f), (int)(ent.boundingBox.Y - 150.0f), 75, 75);
                    Rectangle oTopRight = new Rectangle((int)(ent.boundingBox.X + 150.0f), (int)(ent.boundingBox.Y - 150.0f), 75, 75);
                    Rectangle oBottomLeft = new Rectangle((int)(ent.boundingBox.X - 150.0f), (int)(ent.boundingBox.Y + 150.0f), 75, 75);
                    Rectangle oBottomRight = new Rectangle((int)(ent.boundingBox.X + 150.0f), (int)(ent.boundingBox.Y + 150.0f), 75, 75);

                    if (!collision.moveOffWorld(oTopLeft) && collision.collide(oTopLeft).Count == 0 && collision.collide(topLeft).Count > 0 && ((Player)collision.collide(topLeft)[0]).enemy != ((Player)ent).enemy)
                    {
                        canMove.Add(ent);
                        ent.animate();
                    }
                    else if (!collision.moveOffWorld(oTopRight) && collision.collide(oTopRight).Count == 0 && collision.collide(topRight).Count > 0 && ((Player)collision.collide(topRight)[0]).enemy != ((Player)ent).enemy)
                    {
                        canMove.Add(ent);
                        ent.animate();
                    }
                    else if (!collision.moveOffWorld(oBottomLeft) && collision.collide(oBottomLeft).Count == 0 && collision.collide(bottomLeft).Count > 0 && ((Player)collision.collide(bottomLeft)[0]).enemy != ((Player)ent).enemy)
                    {
                        canMove.Add(ent);
                        ent.animate();
                    }
                    else if (!collision.moveOffWorld(oBottomRight) && collision.collide(oBottomRight).Count == 0 && collision.collide(bottomRight).Count > 0 && ((Player)collision.collide(bottomRight)[0]).enemy != ((Player)ent).enemy)
                    {
                        canMove.Add(ent);
                        ent.animate();
                    }
                }
            }
        }

        public int utility()
        {
            int evaluation = 0;
            
            for (int i = 0; i < player.Count; i++)
            {
                if (!playerTurn && player[i].position != Vector2.Zero)
                    evaluation++;
                if (playerTurn && ai[i].position != Vector2.Zero)
                    evaluation++;
            }

            return (playerTurn) ? 1 : -1;
        }

        public int eval()
        {
            int evaluation = 0;

            generateCanJump();
            if (canMove.Count == 0)
                generateCanMove();

            if (playerTurn)
                evaluation -= canMove.Count * 3;
            else
                evaluation += canMove.Count * 3;

            resetForNextTurn();
            generateCanJump();
            if (canMove.Count == 0)
                generateCanMove();

            if (playerTurn)
                evaluation -= canMove.Count * 3;
            else
                evaluation += canMove.Count * 3;

            resetForNextTurn();

            for (int i = 0; i < player.Count; i++)
            {
                if (player[i].position != Vector2.Zero)
                    evaluation--;
                if (ai[i].position != Vector2.Zero)
                    evaluation++;
            }

            return evaluation;
        }

        public bool gameOver()
        {
            //generateCanJump();
            //if (canMove.Count == 0)
            //    generateCanMove();
            //if (canMove.Count == 0)
            //{
            //    resetCanMove();
            //    playerWin = !playerTurn;
            //    return true;
            //}
            //else
            //{
            //    resetCanMove();
            //    playerTurn = !playerTurn;
            //    generateCanJump();
            //    if (canMove.Count == 0)
            //        generateCanMove();
            //    if (canMove.Count == 0)
            //    {
            //        resetCanMove();
            //        playerTurn = !playerTurn;
            //        playerWin = !playerTurn;
            //        return true;
            //    }
            //    else
            //    {
            //        resetCanMove();
            //        playerTurn = !playerTurn;
            //        return false;
            //    }
            //}

            return canMove.Count == 0;

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            foreach (Entity ent in player)
                if (ent.position != Vector2.Zero)
                    ent.Draw(spriteBatch);
            foreach (Entity ent in ai)
                if (ent.position != Vector2.Zero)
                    ent.Draw(spriteBatch);
            foreach (Entity ent in markers)
                if (ent.position != Vector2.Zero)
                    ent.Draw(spriteBatch);
            spriteBatch.End();
        }
    }
}
