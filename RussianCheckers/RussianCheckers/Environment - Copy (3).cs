﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace RussianCheckers
{
    class Environment : Interface
    {
        
        public List<Entity> player;
        public List<Entity> ai;
        public List<Move> possibleMoves;
        Entity marker;
        int selected;
        public bool playerTurn;
        //int numofMoves;
        public int turn;
        public bool winner = false;

        public struct Move : IComparable
        {
            public Entity piece;
            public List<Entity> markers;
            public List<List<int>> jumpSet;
            public Move(Entity ent, Entity marker)
            {
                piece = ent;
                markers = new List<Entity>();
                for (int i = 0; i < 4; i++)
                    markers.Add(new Entity(marker));
                jumpSet = new List<List<int>>();
                for (int i = 0; i < 4; i++)
                    jumpSet.Add(new List<int>());
            }
            public int CompareTo(object otherMove)
            {
                return ((Move)otherMove).jumpSet[0].Count.CompareTo(this.jumpSet[0].Count);
            }
        }



        //AlphaBeta abs;

        public Environment(Texture2D environment, Rectangle environmentBox, Input inp, Camera cam, Collision colli, Vector2 loc)
            : base(environment, environmentBox, inp, cam, colli, loc)
        {
            player = new List<Entity>();
            ai = new List<Entity>();
            possibleMoves = new List<Move>();
            selected = -1;
            //abs = new AlphaBeta();
        }
        /*
        public Environment(Environment env)
            : base(env)
        {
            player = new List<Entity>();
            player = env.player;
            ai = new List<Entity>();
            ai = env.ai;
            canMove = new List<Entity>();
            foreach (Player ent in env.canMove)
                canMove.Add(new Player(ent));
            playerTurn = env.playerTurn;
            selected = env.selected;
            markers = new List<Entity>();
            foreach (Entity ent in env.markers)
                markers.Add(new Entity(ent));
            jumpSet = new List<List<int>>();
            for (int i = 0; i < 4; i++)
                jumpSet.Add(new List<int>());
        }
        */
        public void LoadContent(ContentManager Content, bool white)
        {
            playerTurn = white;
            string first, second;
            if (white)
            {
                first = "Player1";
                second = "Player2";
                turn = -1;
            }
            else
            {
                first = "Player2";
                second = "Player1";
                turn = 0;
            }
            for (int i = 0; i < 12; i++)
            {
                if (i < 6)
                {
                    if (i < 3)
                        //ai.Add(new Player(Content.Load<Texture2D>(second), 1, 2, 0, 2, new Vector2(positions[0].X + 225.0f / 2.0f + (i * 150) - 75.0f, positions[0].Y + 75.0f / 2.0f + 225.0f), true));
                        ai.Add(new Player(Content.Load<Texture2D>(second), 1, 2, 0, 2, new Vector2(positions[0].X + 225 / 2 + (i * 150), positions[0].Y + 75 / 2), true));
                    else
                        ai.Add(new Player(Content.Load<Texture2D>(second), 1, 2, 0, 2, new Vector2(positions[0].X + 75 / 2 + ((i - 3) * 150), positions[0].Y + 225 / 2), true));
                }
                else
                {
                    if (i < 9)
                        player.Add(new Player(Content.Load<Texture2D>(first), 1, 2, 0, 2, new Vector2(positions[0].X + 225 / 2 + ((i - 6) * 150), positions[0].Y + 675 / 2), false));
                    else
                        player.Add(new Player(Content.Load<Texture2D>(first), 1, 2, 0, 2, new Vector2(positions[0].X + 75 / 2 + ((i - 9) * 150), positions[0].Y + 825 / 2), false));
                }
            }
            marker = new Entity(Content.Load<Texture2D>("PossibleMove"), 1, 1, 0, 0, Vector2.Zero);
            collision.add(player);
            collision.add(ai);
        }

        public void Update(GameTime gameTime)
        {
            base.Update();
            if (!winner)
            {
                if (!playerTurn)
                {
                    generateCanJump();
                    generateJump();

                    DateTime start = DateTime.Now;

                    AlphaBeta alphabeta = new AlphaBeta(this);

                    long elapsedTicks = DateTime.Now.Ticks - start.Ticks;
                    TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);

                    Console.WriteLine("Time " + elapsedSpan.TotalSeconds + " seconds");
                    turn++;
                    //AlphaBeta alphabeta = new AlphaBeta();
                    //Tuple<int, int, int> move = alphabeta.alphabeta(this, new Tuple<int, int, int>(-20, 0, 0), new Tuple<int, int, int>(20, 0, 0), 1);
                    //generateCanJump();
                    //generateJump();
                    //doJump(move.Item2, move.Item3);
                    //possibleMoves[move.Item2].piece.newPosition(possibleMoves[move.Item2].markers[move.Item3].position);
                    //reset();

                    //AlphaBeta alphabeta = new AlphaBeta();
                    //Tuple<int, int, int> move = alphabeta.alphabeta(this, 100, new Tuple<int, int, int>(-20, 0, 0), new Tuple<int, int, int>(20, 0, 0));

                    //generateCanJump();
                    //if (canMove.Count == 0)
                    //    generateCanMove();
                    //selected = canMove[0];
                    //resetCanMove();
                    //selected.animate();
                    //generateJump(selected);
                    //if (markers[0].position == Vector2.Zero)
                    //    generateMove();
                    //doJump(0);
                    //selected.newPosition(markers[0].position);
                    //selected.animate();
                    //selected = null;
                    //resetMarkers();
                    //resetJump();                
                }
                else// if (input.keyPressed(Keys.Enter))
                {
                    //DateTime current = DateTime.Now;

                    //AlphaBeta alphabeta = new AlphaBeta(this);

                    //long elapsedTicks = DateTime.Now.Ticks - current.Ticks;
                    //TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);
                    //Console.WriteLine("Time: " + elapsedSpan.TotalSeconds + " seconds");

                    //AlphaBeta alphabeta = new AlphaBeta();
                    //Tuple<int, int, int> move = alphabeta.alphabeta(this, new Tuple<int, int, int>(-20, 0, 0), new Tuple<int, int, int>(20, 0, 0), 1);
                    //generateCanJump();
                    //generateJump();
                    //doJump(move.Item2, move.Item3);
                    //possibleMoves[move.Item2].piece.newPosition(possibleMoves[move.Item2].markers[move.Item3].position);
                    //reset();

                    if (possibleMoves.Count == 0)
                    {
                        generateCanJump();
                        generateJump();
                        if (possibleMoves.Count == 0)
                            winner = true;
                    }
                    for (int i = 0; i < possibleMoves.Count; i++)
                    {
                        if (input.lMouseBottonClickedOn(possibleMoves[i].piece))
                        {
                            selected = i;
                            break;
                        }
                    }
                    if (selected != -1)
                    {
                        for (int i = 0; i < possibleMoves[selected].markers.Count && possibleMoves[selected].markers[i].position != Vector2.Zero; i++)
                        {
                            if (input.lMouseBottonClickedOn(possibleMoves[selected].markers[i]))
                            {
                                doJump(selected, i);
                                possibleMoves[selected].piece.newPosition(possibleMoves[selected].markers[i].position);
                                reset();
                                turn++;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                if (playerTurn)
                    text[1] = "Menu \n Computer wins";
                else
                    text[1] = "Menu \n Player wins";
            }
        }

        public void reset()
        {
            selected = -1;

            for (int i = 0; i < possibleMoves.Count; i++)
                if (possibleMoves[i].piece.frame.Item2 == 1)
                    possibleMoves[i].piece.animate();

            possibleMoves.Clear();
            playerTurn = !playerTurn;
        }

        public void doJump(int piece, int jumpIdx)
        {
            Entity temp = new Entity(possibleMoves[piece].piece);
            foreach (int jump in possibleMoves[piece].jumpSet[jumpIdx])
            {
                Rectangle topLeft = new Rectangle((int)(temp.boundingBox.X - 75.0f), (int)(temp.boundingBox.Y - 75.0f), 75, 75);
                Rectangle topRight = new Rectangle((int)(temp.boundingBox.X + 75.0f), (int)(temp.boundingBox.Y - 75.0f), 75, 75);
                Rectangle bottomLeft = new Rectangle((int)(temp.boundingBox.X - 75.0f), (int)(temp.boundingBox.Y + 75.0f), 75, 75);
                Rectangle bottomRight = new Rectangle((int)(temp.boundingBox.X + 75.0f), (int)(temp.boundingBox.Y + 75.0f), 75, 75);
                Rectangle oTopLeft = new Rectangle((int)(temp.boundingBox.X - 150.0f), (int)(temp.boundingBox.Y - 150.0f), 75, 75);
                Rectangle oTopRight = new Rectangle((int)(temp.boundingBox.X + 150.0f), (int)(temp.boundingBox.Y - 150.0f), 75, 75);
                Rectangle oBottomLeft = new Rectangle((int)(temp.boundingBox.X - 150.0f), (int)(temp.boundingBox.Y + 150.0f), 75, 75);
                Rectangle oBottomRight = new Rectangle((int)(temp.boundingBox.X + 150.0f), (int)(temp.boundingBox.Y + 150.0f), 75, 75);
                switch (jump)
                {
                    case 1:
                        collision.collide(topLeft)[0].newPosition(Vector2.Zero);
                        temp.newPosition(new Vector2(oTopLeft.Center.X, oTopLeft.Center.Y));
                        break;
                    case 2:
                        collision.collide(topRight)[0].newPosition(Vector2.Zero);
                        temp.newPosition(new Vector2(oTopRight.Center.X, oTopRight.Center.Y));
                        break;
                    case 3:
                        collision.collide(bottomLeft)[0].newPosition(Vector2.Zero);
                        temp.newPosition(new Vector2(oBottomLeft.Center.X, oBottomLeft.Center.Y));
                        break;
                    case 4:
                        collision.collide(bottomRight)[0].newPosition(Vector2.Zero);
                        temp.newPosition(new Vector2(oBottomRight.Center.X, oBottomRight.Center.Y));
                        break;
                }
            }
        }

        void generateMove()
        {
            for (int i = 0; i < possibleMoves.Count; i++)
            {
                int markerIndex = 0;
                if (possibleMoves[i].markers[0].position == Vector2.Zero)
                {
                    if (playerTurn)
                    {
                        Rectangle topLeft = new Rectangle((int)(possibleMoves[i].piece.boundingBox.X - 75.0f), (int)(possibleMoves[i].piece.boundingBox.Y - 75.0f), 75, 75);
                        Rectangle topRight = new Rectangle((int)(possibleMoves[i].piece.boundingBox.X + 75.0f), (int)(possibleMoves[i].piece.boundingBox.Y - 75.0f), 75, 75);

                        if (!collision.moveOffWorld(topLeft) && collision.collide(topLeft).Count == 0)
                            possibleMoves[i].markers[markerIndex++].newPosition(new Vector2(topLeft.Center.X, topLeft.Center.Y));
                        if (!collision.moveOffWorld(topRight) && collision.collide(topRight).Count == 0)
                            possibleMoves[i].markers[markerIndex++].newPosition(new Vector2(topRight.Center.X, topRight.Center.Y));
                    }
                    else
                    {
                        Rectangle bottomLeft = new Rectangle((int)(possibleMoves[i].piece.boundingBox.X - 75.0f), (int)(possibleMoves[i].piece.boundingBox.Y + 75.0f), 75, 75);
                        Rectangle bottomRight = new Rectangle((int)(possibleMoves[i].piece.boundingBox.X + 75.0f), (int)(possibleMoves[i].piece.boundingBox.Y + 75.0f), 75, 75);

                        if (!collision.moveOffWorld(bottomLeft) && collision.collide(bottomLeft).Count == 0)
                            possibleMoves[i].markers[markerIndex++].newPosition(new Vector2(bottomLeft.Center.X, bottomLeft.Center.Y));
                        if (!collision.moveOffWorld(bottomRight) && collision.collide(bottomRight).Count == 0)
                            possibleMoves[i].markers[markerIndex++].newPosition(new Vector2(bottomRight.Center.X, bottomRight.Center.Y));
                    }
                }
            }

            //numofMoves = possibleMoves.Count;
            //for (int i = 0; i < numofMoves; i++)
            //{
            //    for (int j = 1; j < possibleMoves[i].markers.Count && possibleMoves[i].markers[j].position != Vector2.Zero; j++)
            //    {
            //        possibleMoves.Add(new Move(possibleMoves[i].piece, marker));
            //        possibleMoves[possibleMoves.Count - 1].markers[0].newPosition(possibleMoves[i].markers[j].position);
            //        for (int k = 0; k < possibleMoves[i].jumpSet[j].Count; k++ )
            //            possibleMoves[possibleMoves.Count - 1].jumpSet[0].Add(possibleMoves[i].jumpSet[j][k]);
            //    }
            //}
            //possibleMoves.Sort();
        }

        public void generateJump()
        {
            for (int i = 0; i < possibleMoves.Count; i++ )
            {
                Entity ent = possibleMoves[i].piece;
                for (int j = 0; j < 2; j++)
                {
                    Rectangle topLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                    Rectangle topRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                    Rectangle bottomLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                    Rectangle bottomRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                    Rectangle oTopLeft = new Rectangle((int)(ent.boundingBox.X - 150.0f), (int)(ent.boundingBox.Y - 150.0f), 75, 75);
                    Rectangle oTopRight = new Rectangle((int)(ent.boundingBox.X + 150.0f), (int)(ent.boundingBox.Y - 150.0f), 75, 75);
                    Rectangle oBottomLeft = new Rectangle((int)(ent.boundingBox.X - 150.0f), (int)(ent.boundingBox.Y + 150.0f), 75, 75);
                    Rectangle oBottomRight = new Rectangle((int)(ent.boundingBox.X + 150.0f), (int)(ent.boundingBox.Y + 150.0f), 75, 75);

                    bool jumpTopLeft = !collision.moveOffWorld(oTopLeft) && collision.collide(oTopLeft).Count == 0 && collision.collide(topLeft).Count > 0 && ((Player)collision.collide(topLeft)[0]).enemy == playerTurn;
                    bool jumpTopRight = !collision.moveOffWorld(oTopRight) && collision.collide(oTopRight).Count == 0 && collision.collide(topRight).Count > 0 && ((Player)collision.collide(topRight)[0]).enemy == playerTurn;
                    bool jumpBottomLeft = !collision.moveOffWorld(oBottomLeft) && collision.collide(oBottomLeft).Count == 0 && collision.collide(bottomLeft).Count > 0 && ((Player)collision.collide(bottomLeft)[0]).enemy == playerTurn;
                    bool jumpBottomRight = !collision.moveOffWorld(oBottomRight) && collision.collide(oBottomRight).Count == 0 && collision.collide(bottomRight).Count > 0 && ((Player)collision.collide(bottomRight)[0]).enemy == playerTurn;

                    int result;
                    result = possibleMoves[i].markers.FindIndex(
                        delegate(Entity marker)
                        {
                            return marker.position == ent.position;
                        });
                    if (result == -1)
                        result = possibleMoves[i].markers.FindIndex(
                        delegate(Entity marker)
                        {
                            return marker.position == Vector2.Zero;
                        });

                    if (jumpTopLeft)
                    {
                        if (result != -1)
                        {
                            if (j == 1 && possibleMoves[i].jumpSet[result].Count == 0 && result > 0)
                                possibleMoves[i].jumpSet[result].Add(possibleMoves[i].jumpSet[result - 1][0]);
                            possibleMoves[i].jumpSet[result].Add(1);
                            possibleMoves[i].markers[result].newPosition(new Vector2(oTopLeft.Center.X, oTopLeft.Center.Y));
                            collision.add(new Entity(possibleMoves[i].markers[result]));
                            ent = possibleMoves[i].markers[result];
                            result = possibleMoves[i].markers.FindIndex(
                                delegate(Entity marker)
                                {
                                    return marker.position == Vector2.Zero;
                                });
                        }
                    }
                    if (jumpTopRight)
                    {
                        if (result != -1)
                        {
                            if (j == 1 && possibleMoves[i].jumpSet[result].Count == 0 && result > 0)
                                possibleMoves[i].jumpSet[result].Add(possibleMoves[i].jumpSet[result - 1][0]);
                            possibleMoves[i].jumpSet[result].Add(2);
                            possibleMoves[i].markers[result].newPosition(new Vector2(oTopRight.Center.X, oTopRight.Center.Y));
                            collision.add(new Entity(possibleMoves[i].markers[result]));
                            ent = possibleMoves[i].markers[result];
                            result = possibleMoves[i].markers.FindIndex(
                                delegate(Entity marker)
                                {
                                    return marker.position == Vector2.Zero;
                                });
                        }
                    }
                    if (jumpBottomLeft)
                    {
                        if (result != -1)
                        {
                            if (j == 1 && possibleMoves[i].jumpSet[result].Count == 0 && result > 0)
                                possibleMoves[i].jumpSet[result].Add(possibleMoves[i].jumpSet[result - 1][0]);
                            possibleMoves[i].jumpSet[result].Add(3);
                            possibleMoves[i].markers[result].newPosition(new Vector2(oBottomLeft.Center.X, oBottomLeft.Center.Y));
                            collision.add(new Entity(possibleMoves[i].markers[result]));
                            ent = possibleMoves[i].markers[result];
                            result = possibleMoves[i].markers.FindIndex(
                                delegate(Entity marker)
                                {
                                    return marker.position == Vector2.Zero;
                                });
                        }
                    }
                    if (jumpBottomRight)
                    {
                        if (result != -1)
                        {
                            if (j == 1 && possibleMoves[i].jumpSet[result].Count == 0 && result > 0)
                                possibleMoves[i].jumpSet[result].Add(possibleMoves[i].jumpSet[result - 1][0]);
                            possibleMoves[i].jumpSet[result].Add(4);
                            possibleMoves[i].markers[result].newPosition(new Vector2(oBottomRight.Center.X, oBottomRight.Center.Y));
                            collision.add(new Entity(possibleMoves[i].markers[result]));
                            ent = possibleMoves[i].markers[result];
                            result = possibleMoves[i].markers.FindIndex(
                                delegate(Entity marker)
                                {
                                    return marker.position == Vector2.Zero;
                                });
                        }
                    }
                }
                int count = 0;
                foreach (List<int> jump in possibleMoves[i].jumpSet)
                    count += jump.Count;

                if (count == 3 && possibleMoves[i].jumpSet[0][possibleMoves[i].jumpSet[0].Count - 1] == possibleMoves[i].jumpSet[1][possibleMoves[i].jumpSet[1].Count - 1])
                {
                    bool circular = false;
                    switch (possibleMoves[i].jumpSet[1][0])
                    {
                        case 1:
                            circular = collision.collide(new Rectangle(possibleMoves[i].markers[0].boundingBox.X - 75, possibleMoves[i].markers[0].boundingBox.Y - 75, 75, 75)).Count > 0;
                            break;
                        case 2:
                            circular = collision.collide(new Rectangle(possibleMoves[i].markers[0].boundingBox.X + 75, possibleMoves[i].markers[0].boundingBox.Y - 75, 75, 75)).Count > 0;
                            break;
                        case 3:
                            circular = collision.collide(new Rectangle(possibleMoves[i].markers[0].boundingBox.X - 75, possibleMoves[i].markers[0].boundingBox.Y + 75, 75, 75)).Count > 0;
                            break;
                        case 4:
                            circular = collision.collide(new Rectangle(possibleMoves[i].markers[0].boundingBox.X + 75, possibleMoves[i].markers[0].boundingBox.Y + 75, 75, 75)).Count > 0;
                            break;
                    }
                    if (circular)
                    {
                        possibleMoves[i].jumpSet[0].Add(possibleMoves[i].jumpSet[1][0]);
                        switch (possibleMoves[i].jumpSet[1][1])
                        {
                            case 1:
                                possibleMoves[i].jumpSet[0].Add(4);
                                break;
                            case 2:
                                possibleMoves[i].jumpSet[0].Add(3);
                                break;
                            case 3:
                                possibleMoves[i].jumpSet[0].Add(2);
                                break;
                            case 4:
                                possibleMoves[i].jumpSet[0].Add(1);
                                break;
                        }
                        int idx = 0;
                        foreach (int j in possibleMoves[i].jumpSet[0])
                            idx += j;
                        possibleMoves[i].jumpSet[0].Add(10 - idx);

                        possibleMoves[i].jumpSet[1] = new List<int>();
                        possibleMoves[i].markers[1].newPosition(Vector2.Zero);
                        possibleMoves[i].markers[0].newPosition(possibleMoves[i].piece.position);
                    }
                }
                while (collision.size() > 12)
                    collision.remove(collision.size() - 1);
            }
            generateMove();
        }

        void generateCanMove()
        {
            if (playerTurn)
            {
                foreach (Entity ent in player)
                {
                    if (ent.position != Vector2.Zero)
                    {
                        Rectangle topLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                        Rectangle topRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);

                        if (!collision.moveOffWorld(topLeft) && collision.collide(topLeft).Count == 0)
                        {
                            possibleMoves.Add(new Move(ent, marker));
                            ent.animate();
                        }
                        else if (!collision.moveOffWorld(topRight) && collision.collide(topRight).Count == 0)
                        {
                            possibleMoves.Add(new Move(ent, marker));
                            ent.animate();
                        }
                    }
                }
            }
            else
            {
                foreach (Entity ent in ai)
                {
                    if (ent.position != Vector2.Zero)
                    {
                        Rectangle bottomLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                        Rectangle bottomRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);

                        if (!collision.moveOffWorld(bottomLeft) && collision.collide(bottomLeft).Count == 0)
                        {
                            possibleMoves.Add(new Move(ent, marker));
                            ent.animate();
                        }
                        else if (!collision.moveOffWorld(bottomRight) && collision.collide(bottomRight).Count == 0)
                        {
                            possibleMoves.Add(new Move(ent, marker));
                            ent.animate();
                        }
                    }
                }
            }
        }

        public void generateCanJump()
        {

            List<Entity> temp = new List<Entity>();
            
            if (playerTurn)
                temp = player;
            else
                temp = ai;

            foreach (Entity ent in temp)
            {
                if (ent.position != Vector2.Zero)
                {
                    Rectangle topLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                    Rectangle topRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                    Rectangle bottomLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                    Rectangle bottomRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                    Rectangle oTopLeft = new Rectangle((int)(ent.boundingBox.X - 150.0f), (int)(ent.boundingBox.Y - 150.0f), 75, 75);
                    Rectangle oTopRight = new Rectangle((int)(ent.boundingBox.X + 150.0f), (int)(ent.boundingBox.Y - 150.0f), 75, 75);
                    Rectangle oBottomLeft = new Rectangle((int)(ent.boundingBox.X - 150.0f), (int)(ent.boundingBox.Y + 150.0f), 75, 75);
                    Rectangle oBottomRight = new Rectangle((int)(ent.boundingBox.X + 150.0f), (int)(ent.boundingBox.Y + 150.0f), 75, 75);

                    if (!collision.moveOffWorld(oTopLeft) && collision.collide(oTopLeft).Count == 0 && collision.collide(topLeft).Count > 0 && ((Player)collision.collide(topLeft)[0]).enemy != ((Player)ent).enemy)
                    {
                        possibleMoves.Add(new Move(ent, marker));
                        ent.animate();
                    }
                    else if (!collision.moveOffWorld(oTopRight) && collision.collide(oTopRight).Count == 0 && collision.collide(topRight).Count > 0 && ((Player)collision.collide(topRight)[0]).enemy != ((Player)ent).enemy)
                    {
                        possibleMoves.Add(new Move(ent, marker));
                        ent.animate();
                    }
                    else if (!collision.moveOffWorld(oBottomLeft) && collision.collide(oBottomLeft).Count == 0 && collision.collide(bottomLeft).Count > 0 && ((Player)collision.collide(bottomLeft)[0]).enemy != ((Player)ent).enemy)
                    {
                        possibleMoves.Add(new Move(ent, marker));
                        ent.animate();
                    }
                    else if (!collision.moveOffWorld(oBottomRight) && collision.collide(oBottomRight).Count == 0 && collision.collide(bottomRight).Count > 0 && ((Player)collision.collide(bottomRight)[0]).enemy != ((Player)ent).enemy)
                    {
                        possibleMoves.Add(new Move(ent, marker));
                        ent.animate();
                    }
                }
            }
            if (possibleMoves.Count == 0)
                generateCanMove();
        }

        public int utility()
        {
            if (playerTurn && gameOver())
                return 100000;
            else if(gameOver())
                return -100000;

            //int best = int.MinValue;
            int count = 0;
            //if (possibleMoves[0].jumpSet[0].Count > 0)
            //{
            //    for (int i = 0; i < possibleMoves.Count; i++)
            //    {
            //        for (int j = 0; j < possibleMoves[i].markers.Count && possibleMoves[i].markers[j].position != Vector2.Zero; j++)
            //        {
            //            for (int k = 0; k < player.Count; k++)
            //            {
            //                if (player[k].position != Vector2.Zero)
            //                    count -= 4;
            //                if (ai[k].position != Vector2.Zero)
            //                    count += 4;
            //            }

            //            if (playerTurn)
            //                count -= 6;
            //            else
            //                count += 6;
            //            if (count > best)
            //                best = count;
            //        }
            //    }
            //}
            //else
            //{
            //    for (int k = 0; k < player.Count; k++)
            //    {
            //        if (player[k].position != Vector2.Zero)
            //            count -= 4;
            //        if (ai[k].position != Vector2.Zero)
            //            count += 4;
            //    }
            //    if (playerTurn)
            //        count -= 6;
            //    else
            //        count += 6;
            //    best = count;
            //}
            //return best;
            if (possibleMoves[0].jumpSet[0].Count != 0)
            {
                for (int k = 0; k < player.Count; k++)
                {
                    if (player[k].position != Vector2.Zero)
                        count -= 2;
                    if (ai[k].position != Vector2.Zero)
                        count += 2;
                }
                if (playerTurn)
                {
                    for (int i = 0; i < possibleMoves.Count; i++)
                    {
                        for (int j = 0; j < possibleMoves[i].jumpSet.Count; j++)
                            count -= 2 * possibleMoves[i].jumpSet[0].Count;
                    }
                }
                else
                {
                    for (int i = 0; i < possibleMoves.Count; i++)
                    {
                        for (int j = 0; j < possibleMoves[i].jumpSet.Count; j++)
                            count += 2 * possibleMoves[i].jumpSet[0].Count;
                    }
                }
            }
            else
            {
                for (int k = 0; k < player.Count; k++)
                {
                    if (player[k].position != Vector2.Zero)
                        count -= 3;
                    if (ai[k].position != Vector2.Zero)
                        count += 3;
                }
            }
            return count;
        }

        public bool gameOver()
        {
            return possibleMoves.Count == 0;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            foreach (Entity ent in player)
                if (ent.position != Vector2.Zero)
                    ent.Draw(spriteBatch);
            foreach (Entity ent in ai)
                if (ent.position != Vector2.Zero)
                    ent.Draw(spriteBatch);
            if (selected != -1)
            {
                foreach (Entity ent in possibleMoves[selected].markers)
                    if (ent.position != Vector2.Zero)
                        ent.Draw(spriteBatch);
            }
            spriteBatch.End();
        }
    }
}
