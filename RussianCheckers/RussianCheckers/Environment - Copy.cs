﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace RussianCheckers
{
    class Environment : Interface
    {
        List<Entity> player;
        List<Entity> ai;
        List<Entity> canMove;
        bool playerTurn;
        Entity selected;
        List<Entity> markers;

        public Environment(Texture2D environment, Rectangle environmentBox, Input inp, Camera cam, Collision colli, Vector2 loc)
            : base(environment, environmentBox, inp, cam, colli, loc)
        {
            player = new List<Entity>();
            ai = new List<Entity>();
            canMove = new List<Entity>();
            markers = new List<Entity>();
            playerTurn = true;
            selected = null;
        }

        public void LoadContent(ContentManager Content)
        {
            for (int i = 0; i < 12; i++)
            {
                if (i < 6)
                {
                    if (i < 3)
                        ai.Add(new Player(Content.Load<Texture2D>("Player1"), 1, 2, 0, 2, new Vector2(positions[0].X + 225.0f / 2.0f + (i * 150) - 75.0f, positions[0].Y + 75.0f / 2.0f + 225.0f), true));
                    else
                        ai.Add(new Player(Content.Load<Texture2D>("Player1"), 1, 2, 0, 2, new Vector2(positions[0].X + 75.0f / 2.0f + ((i - 3) * 150), positions[0].Y + 225.0f / 2.0f), true));
                }
                else
                {
                    if (i < 9)
                        player.Add(new Player(Content.Load<Texture2D>("Player2"), 1, 2, 0, 2, new Vector2(positions[0].X + 225.0f / 2.0f + ((i - 6) * 150), positions[0].Y + 675.0f / 2.0f), false));
                    else
                        player.Add(new Player(Content.Load<Texture2D>("Player2"), 1, 2, 0, 2, new Vector2(positions[0].X + 75.0f / 2.0f + ((i - 9) * 150), positions[0].Y + 825.0f / 2.0f), false));
                }
            }
            for (int i = 0; i < 4; i++)
            {
                markers.Add(new Entity(Content.Load<Texture2D>("PossibleMove"), 1, 1, 0, 0, Vector2.Zero));
            }
            collision.add(player);
            collision.add(ai);
        }

        public override void Update()
        {
            base.Update();
            if (selected == null && canMove.Count == 0)
            {
                generateCanJump();
                if (canMove.Count == 0)
                    generateCanMove();
            }
            foreach (Entity ent in canMove)
            {
                if (input.lMouseBottonClickedOn(ent))
                {
                    selected = ent;
                    resetCanMove();
                    selected.animate();
                    break;
                }
            }
            if (selected != null)
            {
                generateJump(selected);
                if(markers[0].position == Vector2.Zero)
                    generateMove();
            }
            foreach (Entity ent in markers)
            {
                if (input.lMouseBottonClickedOn(ent))
                {
                    selected.newPosition(ent.position);
                    selected.animate();
                    selected = null;
                    resetMarkers();
                    playerTurn = !playerTurn;
                }
            }
        }

        void resetCanMove()
        {
            foreach (Entity ent in canMove)
                ent.animate();
            canMove.Clear();
        }

        void resetMarkers()
        {
            foreach (Entity ent in markers)
                ent.newPosition(Vector2.Zero);
        }

        void generateMove()
        {
            int markerIndex = 0;
            if (playerTurn)
            {

                Rectangle topLeft = new Rectangle((int)(selected.boundingBox.X - 75.0f), (int)(selected.boundingBox.Y - 75.0f), 75, 75);
                Rectangle topRight = new Rectangle((int)(selected.boundingBox.X + 75.0f), (int)(selected.boundingBox.Y - 75.0f), 75, 75);

                if (!collision.moveOffWorld(topLeft) && collision.collide(topLeft).Count == 0)
                    markers[markerIndex++].newPosition(new Vector2(topLeft.Center.X, topLeft.Center.Y));
                if (!collision.moveOffWorld(topRight) && collision.collide(topRight).Count == 0)
                    markers[markerIndex++].newPosition(new Vector2(topRight.Center.X, topRight.Center.Y));
            }
            else
            {

                Rectangle bottomLeft = new Rectangle((int)(selected.boundingBox.X - 75.0f), (int)(selected.boundingBox.Y + 75.0f), 75, 75);
                Rectangle bottomRight = new Rectangle((int)(selected.boundingBox.X + 75.0f), (int)(selected.boundingBox.Y + 75.0f), 75, 75);

                if (!collision.moveOffWorld(bottomLeft) && collision.collide(bottomLeft).Count == 0)
                    markers[markerIndex++].newPosition(new Vector2(bottomLeft.Center.X, bottomLeft.Center.Y));
                if (!collision.moveOffWorld(bottomRight) && collision.collide(bottomRight).Count == 0)
                    markers[markerIndex++].newPosition(new Vector2(bottomRight.Center.X, bottomRight.Center.Y));
            }
        }

        void generateJump(Entity ent)
        {
            for (int i = 0; i < 4; i++ )
            {
                Rectangle topLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                Rectangle topRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                Rectangle bottomLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                Rectangle bottomRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                Rectangle oTopLeft = new Rectangle((int)(ent.boundingBox.X - 150.0f), (int)(ent.boundingBox.Y - 150.0f), 75, 75);
                Rectangle oTopRight = new Rectangle((int)(ent.boundingBox.X + 150.0f), (int)(ent.boundingBox.Y - 150.0f), 75, 75);
                Rectangle oBottomLeft = new Rectangle((int)(ent.boundingBox.X - 150.0f), (int)(ent.boundingBox.Y + 150.0f), 75, 75);
                Rectangle oBottomRight = new Rectangle((int)(ent.boundingBox.X + 150.0f), (int)(ent.boundingBox.Y + 150.0f), 75, 75);

                int result;
                result = markers.FindIndex(
                    delegate(Entity marker)
                    {
                        return marker.position == ent.position;
                    });
                if(result == -1)
                    result = markers.FindIndex(
                    delegate(Entity marker)
                    {
                        return marker.position == Vector2.Zero;
                    });

                if (!collision.moveOffWorld(oTopLeft) && collision.collide(oTopLeft).Count == 0 && collision.collide(topLeft).Count > 0 && ((Player)collision.collide(topLeft)[0]).enemy != ((Player)selected).enemy)
                {
                    if (result != -1)
                    {
                        markers[result].newPosition(new Vector2(oTopLeft.Center.X, oTopLeft.Center.Y));
                        collision.add(new Entity(markers[result]));
                        ent = markers[result];
                        result = markers.FindIndex(
                            delegate(Entity marker)
                            {
                                return marker.position == Vector2.Zero;
                            });
                    }
                }
                if (!collision.moveOffWorld(oTopRight) && collision.collide(oTopRight).Count == 0 && collision.collide(topRight).Count > 0 && ((Player)collision.collide(topRight)[0]).enemy != ((Player)selected).enemy)
                {
                    if (result != -1)
                    {
                        markers[result].newPosition(new Vector2(oTopRight.Center.X, oTopRight.Center.Y));
                        collision.add(new Entity(markers[result]));
                        ent = markers[result];
                        result = markers.FindIndex(
                            delegate(Entity marker)
                            {
                                return marker.position == Vector2.Zero;
                            });
                    }
                }
                if (!collision.moveOffWorld(oBottomLeft) && collision.collide(oBottomLeft).Count == 0 && collision.collide(bottomLeft).Count > 0 && ((Player)collision.collide(bottomLeft)[0]).enemy != ((Player)selected).enemy)
                {
                    if (result != -1)
                    {
                        markers[result].newPosition(new Vector2(oBottomLeft.Center.X, oBottomLeft.Center.Y));
                        collision.add(new Entity(markers[result]));
                        ent = markers[result];
                        result = markers.FindIndex(
                            delegate(Entity marker)
                            {
                                return marker.position == Vector2.Zero;
                            });
                    }
                }
                if (!collision.moveOffWorld(oBottomRight) && collision.collide(oBottomRight).Count == 0 && collision.collide(bottomRight).Count > 0 && ((Player)collision.collide(bottomRight)[0]).enemy != ((Player)selected).enemy)
                {
                    if (result != -1)
                    {
                        markers[result].newPosition(new Vector2(oBottomRight.Center.X, oBottomRight.Center.Y));
                        collision.add(new Entity(markers[result]));
                        ent = markers[result];
                        result = markers.FindIndex(
                            delegate(Entity marker)
                            {
                                return marker.position == Vector2.Zero;
                            });
                    }
                }
            }
        }

        void generateCanMove()
        {
            if (playerTurn)
            {
                foreach (Entity ent in player)
                {
                    Rectangle topLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                    Rectangle topRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);

                    if (!collision.moveOffWorld(topLeft) && collision.collide(topLeft).Count == 0)
                    {
                        canMove.Add(ent);
                        ent.animate();
                    }
                    else if (!collision.moveOffWorld(topRight) && collision.collide(topRight).Count == 0)
                    {
                        canMove.Add(ent);
                        ent.animate();
                    }
                }
            }
            else
            {
                foreach (Entity ent in ai)
                {
                    Rectangle bottomLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                    Rectangle bottomRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);

                    if (!collision.moveOffWorld(bottomLeft) && collision.collide(bottomLeft).Count == 0)
                    {
                        canMove.Add(ent);
                        ent.animate();
                    }
                    else if (!collision.moveOffWorld(bottomRight) && collision.collide(bottomRight).Count == 0)
                    {
                        canMove.Add(ent);
                        ent.animate();
                    }
                }
            }
        }

        void generateCanJump()
        {

            List<Entity> temp = new List<Entity>();
            
            if (playerTurn)
                temp = player;
            else
                temp = ai;

            foreach (Entity ent in temp)
            {
                Rectangle topLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                Rectangle topRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                Rectangle bottomLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                Rectangle bottomRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                Rectangle oTopLeft = new Rectangle((int)(ent.boundingBox.X - 150.0f), (int)(ent.boundingBox.Y - 150.0f), 75, 75);
                Rectangle oTopRight = new Rectangle((int)(ent.boundingBox.X + 150.0f), (int)(ent.boundingBox.Y - 150.0f), 75, 75);
                Rectangle oBottomLeft = new Rectangle((int)(ent.boundingBox.X - 150.0f), (int)(ent.boundingBox.Y + 150.0f), 75, 75);
                Rectangle oBottomRight = new Rectangle((int)(ent.boundingBox.X + 150.0f), (int)(ent.boundingBox.Y + 150.0f), 75, 75);

                if(!collision.moveOffWorld(oTopLeft) && collision.collide(oTopLeft).Count == 0 && collision.collide(topLeft).Count > 0 && ((Player)collision.collide(topLeft)[0]).enemy != ((Player)ent).enemy)
                {
                    canMove.Add(ent);
                    ent.animate();
                }
                else if(!collision.moveOffWorld(oTopRight) && collision.collide(oTopRight).Count == 0 && collision.collide(topRight).Count > 0 && ((Player)collision.collide(topRight)[0]).enemy != ((Player)ent).enemy)
                {
                    canMove.Add(ent);
                    ent.animate();
                }
                else if (!collision.moveOffWorld(oBottomLeft) && collision.collide(oBottomLeft).Count == 0 && collision.collide(bottomLeft).Count > 0 && ((Player)collision.collide(bottomLeft)[0]).enemy != ((Player)ent).enemy)
                {
                    canMove.Add(ent);
                    ent.animate();
                }
                else if (!collision.moveOffWorld(oBottomRight) && collision.collide(oBottomRight).Count == 0 && collision.collide(bottomRight).Count > 0 && ((Player)collision.collide(bottomRight)[0]).enemy != ((Player)ent).enemy)
                {
                    canMove.Add(ent);
                    ent.animate();
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            foreach (Entity ent in player)
                ent.Draw(spriteBatch);
            foreach (Entity ent in ai)
                ent.Draw(spriteBatch);
            foreach (Entity ent in markers)
                if (ent.position != Vector2.Zero)
                    ent.Draw(spriteBatch);
            spriteBatch.End();
        }
    }
}
