﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace RussianCheckers
{
    class Environment : Interface
    {
        
        public List<Entity> player;         //list of players pieces
        public List<Entity> ai;             //list of ai pieces
        public List<Move> possibleMoves;    //list of all possible moves for current player moving
        Entity marker;                      //marker for showing possible moves
        int selected;                       //show which piece the player selected
        public bool playerTurn;             //players turn or not
        //int numofMoves;
        public int turn;                    //number of moves ai performed
        public bool winner = false;         //there was a winner
        public int aiLevel;                 //ai difficulty

        //A move is a piece, a marker, and the jumpset to get to the marker from the moving pieces location(jumpset is empty if there are no jumps).
        public struct Move : IComparable
        {
            public Entity piece;
            public List<Entity> markers;
            public List<List<int>> jumpSet;
            public Move(Entity ent, Entity marker)
            {
                piece = ent;
                markers = new List<Entity>();
                for (int i = 0; i < 4; i++)
                    markers.Add(new Entity(marker));
                jumpSet = new List<List<int>>();
                for (int i = 0; i < 4; i++)
                    jumpSet.Add(new List<int>());
            }
            public int CompareTo(object otherMove)
            {
                return ((Move)otherMove).jumpSet[0].Count.CompareTo(this.jumpSet[0].Count);
            }
        }
        /// <summary>
        /// Constructor
        /// For more details please look at Interface.cs
        /// </summary>
        public Environment(Texture2D environment, Rectangle environmentBox, Input inp, Camera cam, Collision colli, Vector2 loc)
            : base(environment, environmentBox, inp, cam, colli, loc)
        {
            player = new List<Entity>();
            ai = new List<Entity>();
            possibleMoves = new List<Move>();
            selected = -1;
            turn = 0;
        }

        /// <summary>
        /// Loads sprite textures and place them in the correct location
        /// </summary>
        /// <param name="Content">Content manager required to load sprites</param>
        /// <param name="black">Wether the player is Black or Red</param>
        public void LoadContent(ContentManager Content, bool black)
        {
            playerTurn = black;
            string first, second;
            if (black)
            {
                first = "Player1";
                second = "Player2";
            }
            else
            {
                first = "Player2";
                second = "Player1";
                
            }

            for (int i = 0; i < 12; i++)
            {
                if (i < 6)
                {
                    if (i < 3)
                        //ai.Add(new Player(Content.Load<Texture2D>(second), 1, 2, 0, 2, new Vector2(positions[0].X + 225.0f / 2.0f + (i * 150) - 75.0f, positions[0].Y + 75.0f / 2.0f + 225.0f), true));
                        ai.Add(new Player(Content.Load<Texture2D>(second), 1, 2, 0, 2, new Vector2(positions[0].X + 225 / 2 + (i * 150), positions[0].Y + 75 / 2), true));
                    else
                        ai.Add(new Player(Content.Load<Texture2D>(second), 1, 2, 0, 2, new Vector2(positions[0].X + 75 / 2 + ((i - 3) * 150), positions[0].Y + 225 / 2), true));
                }
                else
                {
                    if (i < 9)
                        player.Add(new Player(Content.Load<Texture2D>(first), 1, 2, 0, 2, new Vector2(positions[0].X + 225 / 2 + ((i - 6) * 150), positions[0].Y + 675 / 2), false));
                    else
                        player.Add(new Player(Content.Load<Texture2D>(first), 1, 2, 0, 2, new Vector2(positions[0].X + 75 / 2 + ((i - 9) * 150), positions[0].Y + 825 / 2), false));
                }
            }
            marker = new Entity(Content.Load<Texture2D>("PossibleMove"), 1, 1, 0, 0, Vector2.Zero);
            collision.add(player);
            collision.add(ai);
            buttons.Clear();
        }

        public void Update(GameTime gameTime)
        {
            base.Update();
            if (!winner)    //no winnner,  not the end of game
            {
                if (!playerTurn)    //ai's turn
                {
                    generateCanJump();      //gemerate list of possible pieces that can jump, if none it will also generate possible pieces that can move
                    generateJump();         //generate list of possible jumps from the possible pieces that can move, if none it will also generate possible moves from the possible pieces that can move
                    AlphaBeta alphabeta = new AlphaBeta(this);  //alpha beta search and performs the action
                    turn++;        //ai made a move so incriment ai move counter
                }
                else        //player's turn
                {
                    if (possibleMoves.Count == 0)
                    {
                        generateCanJump();
                        generateJump();
                        if (possibleMoves.Count == 0) // there is a winner (not the player)
                            winner = true;
                    }
                    for (int i = 0; i < possibleMoves.Count; i++)
                    {
                        if (input.lMouseButtonClickedOn(possibleMoves[i].piece))
                        {
                            selected = i;
                            break;
                        }
                    }
                    if (selected != -1)
                    {
                        for (int i = 0; i < possibleMoves[selected].markers.Count && possibleMoves[selected].markers[i].position != Vector2.Zero; i++)
                        {
                            if (input.lMouseButtonClickedOn(possibleMoves[selected].markers[i]))
                            {
                                doJump(selected, i);
                                possibleMoves[selected].piece.newPosition(possibleMoves[selected].markers[i].position);
                                reset();
                                //turn++;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                if (playerTurn)
                    text[1] = "Computer wins";
                else
                    text[1] = "Player wins";
            }
        }

        /// <summary>
        /// resets possible moves and changes players turn, for next player to go
        /// </summary>
        public void reset()
        {
            selected = -1;

            for (int i = 0; i < possibleMoves.Count; i++)
                if (possibleMoves[i].piece.frame.Item2 == 1)
                    possibleMoves[i].piece.animate();

            possibleMoves.Clear();
            playerTurn = !playerTurn;
        }

        /// <summary>
        /// Perform jump sequence
        /// </summary>
        /// <param name="piece">piece that will perform jump</param>
        /// <param name="jumpIdx">which jump to perform (chance of multiple jumps for a given piece)</param>
        public void doJump(int piece, int jumpIdx)
        {
            Entity temp = new Entity(possibleMoves[piece].piece);
            foreach (int jump in possibleMoves[piece].jumpSet[jumpIdx])
            {
                Rectangle topLeft = new Rectangle((int)(temp.boundingBox.X - 75.0f), (int)(temp.boundingBox.Y - 75.0f), 75, 75);
                Rectangle topRight = new Rectangle((int)(temp.boundingBox.X + 75.0f), (int)(temp.boundingBox.Y - 75.0f), 75, 75);
                Rectangle bottomLeft = new Rectangle((int)(temp.boundingBox.X - 75.0f), (int)(temp.boundingBox.Y + 75.0f), 75, 75);
                Rectangle bottomRight = new Rectangle((int)(temp.boundingBox.X + 75.0f), (int)(temp.boundingBox.Y + 75.0f), 75, 75);
                Rectangle oTopLeft = new Rectangle((int)(temp.boundingBox.X - 150.0f), (int)(temp.boundingBox.Y - 150.0f), 75, 75);
                Rectangle oTopRight = new Rectangle((int)(temp.boundingBox.X + 150.0f), (int)(temp.boundingBox.Y - 150.0f), 75, 75);
                Rectangle oBottomLeft = new Rectangle((int)(temp.boundingBox.X - 150.0f), (int)(temp.boundingBox.Y + 150.0f), 75, 75);
                Rectangle oBottomRight = new Rectangle((int)(temp.boundingBox.X + 150.0f), (int)(temp.boundingBox.Y + 150.0f), 75, 75);
                switch (jump)
                {
                    case 1:
                        collision.collide(topLeft)[0].newPosition(Vector2.Zero);
                        temp.newPosition(new Vector2(oTopLeft.Center.X, oTopLeft.Center.Y));
                        break;
                    case 2:
                        collision.collide(topRight)[0].newPosition(Vector2.Zero);
                        temp.newPosition(new Vector2(oTopRight.Center.X, oTopRight.Center.Y));
                        break;
                    case 3:
                        collision.collide(bottomLeft)[0].newPosition(Vector2.Zero);
                        temp.newPosition(new Vector2(oBottomLeft.Center.X, oBottomLeft.Center.Y));
                        break;
                    case 4:
                        collision.collide(bottomRight)[0].newPosition(Vector2.Zero);
                        temp.newPosition(new Vector2(oBottomRight.Center.X, oBottomRight.Center.Y));
                        break;
                }
            }
        }

        /// <summary>
        /// generate moves for pieces that can move if there was no jumps
        /// </summary>
        void generateMove()
        {
            for (int i = 0; i < possibleMoves.Count; i++)
            {
                int markerIndex = 0;
                if (possibleMoves[i].markers[0].position == Vector2.Zero) // check if there was no jumps
                {
                    if (playerTurn) //player could only move diagaonally up
                    {
                        Rectangle topLeft = new Rectangle((int)(possibleMoves[i].piece.boundingBox.X - 75.0f), (int)(possibleMoves[i].piece.boundingBox.Y - 75.0f), 75, 75);
                        Rectangle topRight = new Rectangle((int)(possibleMoves[i].piece.boundingBox.X + 75.0f), (int)(possibleMoves[i].piece.boundingBox.Y - 75.0f), 75, 75);

                        if (!collision.moveOffWorld(topLeft) && collision.collide(topLeft).Count == 0)
                            possibleMoves[i].markers[markerIndex++].newPosition(new Vector2(topLeft.Center.X, topLeft.Center.Y));
                        if (!collision.moveOffWorld(topRight) && collision.collide(topRight).Count == 0)
                            possibleMoves[i].markers[markerIndex++].newPosition(new Vector2(topRight.Center.X, topRight.Center.Y));
                    }
                    else //ai could only move diagonally down
                    {
                        Rectangle bottomLeft = new Rectangle((int)(possibleMoves[i].piece.boundingBox.X - 75.0f), (int)(possibleMoves[i].piece.boundingBox.Y + 75.0f), 75, 75);
                        Rectangle bottomRight = new Rectangle((int)(possibleMoves[i].piece.boundingBox.X + 75.0f), (int)(possibleMoves[i].piece.boundingBox.Y + 75.0f), 75, 75);

                        if (!collision.moveOffWorld(bottomLeft) && collision.collide(bottomLeft).Count == 0)
                            possibleMoves[i].markers[markerIndex++].newPosition(new Vector2(bottomLeft.Center.X, bottomLeft.Center.Y));
                        if (!collision.moveOffWorld(bottomRight) && collision.collide(bottomRight).Count == 0)
                            possibleMoves[i].markers[markerIndex++].newPosition(new Vector2(bottomRight.Center.X, bottomRight.Center.Y));
                    }
                }
            }

            //my attempt at perfect order (didn't work very well)

            //numofMoves = possibleMoves.Count;
            //for (int i = 0; i < numofMoves; i++)
            //{
            //    for (int j = 1; j < possibleMoves[i].markers.Count && possibleMoves[i].markers[j].position != Vector2.Zero; j++)
            //    {
            //        possibleMoves.Add(new Move(possibleMoves[i].piece, marker));
            //        possibleMoves[possibleMoves.Count - 1].markers[0].newPosition(possibleMoves[i].markers[j].position);
            //        for (int k = 0; k < possibleMoves[i].jumpSet[j].Count; k++)
            //            possibleMoves[possibleMoves.Count - 1].jumpSet[0].Add(possibleMoves[i].jumpSet[j][k]);
            //    }
            //}
            //possibleMoves.Sort();
        }

        /// <summary>
        /// generate list of possible jumps from the possible pieces that can move, if none it will call generateMove()
        /// </summary>
        public void generateJump()
        {
            for (int i = 0; i < possibleMoves.Count; i++ )
            {
                Entity ent = possibleMoves[i].piece;
                for (int j = 0; j < 2; j++)
                {
                    Rectangle topLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                    Rectangle topRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                    Rectangle bottomLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                    Rectangle bottomRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                    Rectangle oTopLeft = new Rectangle((int)(ent.boundingBox.X - 150.0f), (int)(ent.boundingBox.Y - 150.0f), 75, 75);
                    Rectangle oTopRight = new Rectangle((int)(ent.boundingBox.X + 150.0f), (int)(ent.boundingBox.Y - 150.0f), 75, 75);
                    Rectangle oBottomLeft = new Rectangle((int)(ent.boundingBox.X - 150.0f), (int)(ent.boundingBox.Y + 150.0f), 75, 75);
                    Rectangle oBottomRight = new Rectangle((int)(ent.boundingBox.X + 150.0f), (int)(ent.boundingBox.Y + 150.0f), 75, 75);

                    //possible jumps in these directions
                    bool jumpTopLeft = !collision.moveOffWorld(oTopLeft) && collision.collide(oTopLeft).Count == 0 && collision.collide(topLeft).Count > 0 && ((Player)collision.collide(topLeft)[0]).enemy == playerTurn;
                    bool jumpTopRight = !collision.moveOffWorld(oTopRight) && collision.collide(oTopRight).Count == 0 && collision.collide(topRight).Count > 0 && ((Player)collision.collide(topRight)[0]).enemy == playerTurn;
                    bool jumpBottomLeft = !collision.moveOffWorld(oBottomLeft) && collision.collide(oBottomLeft).Count == 0 && collision.collide(bottomLeft).Count > 0 && ((Player)collision.collide(bottomLeft)[0]).enemy == playerTurn;
                    bool jumpBottomRight = !collision.moveOffWorld(oBottomRight) && collision.collide(oBottomRight).Count == 0 && collision.collide(bottomRight).Count > 0 && ((Player)collision.collide(bottomRight)[0]).enemy == playerTurn;

                    //after first jump checks if it can jump again
                    int result;
                    result = possibleMoves[i].markers.FindIndex(
                        delegate(Entity marker)
                        {
                            return marker.position == ent.position;
                        });
                    if (result == -1) // if cant jump again, there might be anoth jump so change markers
                        result = possibleMoves[i].markers.FindIndex(
                        delegate(Entity marker)
                        {
                            return marker.position == Vector2.Zero;
                        });

                    if (jumpTopLeft) //can jump to the top left, set marker there
                    {
                        if (result != -1)
                        {
                            if (j == 1 && possibleMoves[i].jumpSet[result].Count == 0 && result > 0)
                                possibleMoves[i].jumpSet[result].Add(possibleMoves[i].jumpSet[result - 1][0]);
                            possibleMoves[i].jumpSet[result].Add(1);
                            possibleMoves[i].markers[result].newPosition(new Vector2(oTopLeft.Center.X, oTopLeft.Center.Y));
                            collision.add(new Entity(possibleMoves[i].markers[result]));
                            ent = possibleMoves[i].markers[result];
                            result = possibleMoves[i].markers.FindIndex(
                                delegate(Entity marker)
                                {
                                    return marker.position == Vector2.Zero;
                                });
                        }
                    }
                    if (jumpTopRight) //can jump to the top right, set marker there
                    {
                        if (result != -1)
                        {
                            if (j == 1 && possibleMoves[i].jumpSet[result].Count == 0 && result > 0)
                                possibleMoves[i].jumpSet[result].Add(possibleMoves[i].jumpSet[result - 1][0]);
                            possibleMoves[i].jumpSet[result].Add(2);
                            possibleMoves[i].markers[result].newPosition(new Vector2(oTopRight.Center.X, oTopRight.Center.Y));
                            collision.add(new Entity(possibleMoves[i].markers[result]));
                            ent = possibleMoves[i].markers[result];
                            result = possibleMoves[i].markers.FindIndex(
                                delegate(Entity marker)
                                {
                                    return marker.position == Vector2.Zero;
                                });
                        }
                    }
                    if (jumpBottomLeft) //can jump to the bottom left, set marker there
                    {
                        if (result != -1)
                        {
                            if (j == 1 && possibleMoves[i].jumpSet[result].Count == 0 && result > 0)
                                possibleMoves[i].jumpSet[result].Add(possibleMoves[i].jumpSet[result - 1][0]);
                            possibleMoves[i].jumpSet[result].Add(3);
                            possibleMoves[i].markers[result].newPosition(new Vector2(oBottomLeft.Center.X, oBottomLeft.Center.Y));
                            collision.add(new Entity(possibleMoves[i].markers[result]));
                            ent = possibleMoves[i].markers[result];
                            result = possibleMoves[i].markers.FindIndex(
                                delegate(Entity marker)
                                {
                                    return marker.position == Vector2.Zero;
                                });
                        }
                    }
                    if (jumpBottomRight) //can jump to the bottom right, set marker there
                    {
                        if (result != -1)
                        {
                            if (j == 1 && possibleMoves[i].jumpSet[result].Count == 0 && result > 0)
                                possibleMoves[i].jumpSet[result].Add(possibleMoves[i].jumpSet[result - 1][0]);
                            possibleMoves[i].jumpSet[result].Add(4);
                            possibleMoves[i].markers[result].newPosition(new Vector2(oBottomRight.Center.X, oBottomRight.Center.Y));
                            collision.add(new Entity(possibleMoves[i].markers[result]));
                            ent = possibleMoves[i].markers[result];
                            result = possibleMoves[i].markers.FindIndex(
                                delegate(Entity marker)
                                {
                                    return marker.position == Vector2.Zero;
                                });
                        }
                    }
                }
                //special case of circular jump
                int count = 0;
                foreach (List<int> jump in possibleMoves[i].jumpSet)
                    count += jump.Count;

                if (count == 3 && possibleMoves[i].jumpSet[0][possibleMoves[i].jumpSet[0].Count - 1] == possibleMoves[i].jumpSet[1][possibleMoves[i].jumpSet[1].Count - 1])
                {
                    bool circular = false;
                    switch (possibleMoves[i].jumpSet[1][0])
                    {
                        case 1:
                            circular = collision.collide(new Rectangle(possibleMoves[i].markers[0].boundingBox.X - 75, possibleMoves[i].markers[0].boundingBox.Y - 75, 75, 75)).Count > 0;
                            break;
                        case 2:
                            circular = collision.collide(new Rectangle(possibleMoves[i].markers[0].boundingBox.X + 75, possibleMoves[i].markers[0].boundingBox.Y - 75, 75, 75)).Count > 0;
                            break;
                        case 3:
                            circular = collision.collide(new Rectangle(possibleMoves[i].markers[0].boundingBox.X - 75, possibleMoves[i].markers[0].boundingBox.Y + 75, 75, 75)).Count > 0;
                            break;
                        case 4:
                            circular = collision.collide(new Rectangle(possibleMoves[i].markers[0].boundingBox.X + 75, possibleMoves[i].markers[0].boundingBox.Y + 75, 75, 75)).Count > 0;
                            break;
                    }
                    if (circular)
                    {
                        possibleMoves[i].jumpSet[0].Add(possibleMoves[i].jumpSet[1][0]);
                        switch (possibleMoves[i].jumpSet[1][1])
                        {
                            case 1:
                                possibleMoves[i].jumpSet[0].Add(4);
                                break;
                            case 2:
                                possibleMoves[i].jumpSet[0].Add(3);
                                break;
                            case 3:
                                possibleMoves[i].jumpSet[0].Add(2);
                                break;
                            case 4:
                                possibleMoves[i].jumpSet[0].Add(1);
                                break;
                        }
                        int idx = 0;
                        foreach (int j in possibleMoves[i].jumpSet[0])
                            idx += j;
                        possibleMoves[i].jumpSet[0].Add(10 - idx);

                        possibleMoves[i].jumpSet[1] = new List<int>();
                        possibleMoves[i].markers[1].newPosition(Vector2.Zero);
                        possibleMoves[i].markers[0].newPosition(possibleMoves[i].piece.position);
                    }
                }
                while (collision.size() > 12) //reset list if entities in collition back to just pieces (needed to add markes in to create multiple jumps)
                    collision.remove(collision.size() - 1);
            }
            generateMove();
        }

        /// <summary>
        /// generate list of pieces that can move (does not generate list of move for those pieces)
        /// </summary>
        void generateCanMove()
        {
            if (playerTurn) //generate for player since it's player's turn
            {
                foreach (Entity ent in player)
                {
                    if (ent.position != Vector2.Zero) // piece is not captured
                    {
                        Rectangle topLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                        Rectangle topRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);

                        if (!collision.moveOffWorld(topLeft) && collision.collide(topLeft).Count == 0 || !collision.moveOffWorld(topRight) && collision.collide(topRight).Count == 0)
                        {
                            possibleMoves.Add(new Move(ent, marker));
                            ent.animate();
                        }
                    }
                }
            }
            else //generate for ai since it's ai's turn
            {
                foreach (Entity ent in ai)
                {
                    if (ent.position != Vector2.Zero) // piece is not captured
                    {
                        Rectangle bottomLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                        Rectangle bottomRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);

                        if (!collision.moveOffWorld(bottomLeft) && collision.collide(bottomLeft).Count == 0 || !collision.moveOffWorld(bottomRight) && collision.collide(bottomRight).Count == 0)
                        {
                            possibleMoves.Add(new Move(ent, marker));
                            ent.animate();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// gernarte list of pieces that can jump (does not generat list of jumps for those pieces)
        /// </summary>
        public void generateCanJump()
        {

            List<Entity> temp = new List<Entity>();
            
            if (playerTurn)
                temp = player; // player's turn
            else
                temp = ai;  // ai's turn

            foreach (Entity ent in temp)
            {
                if (ent.position != Vector2.Zero)
                {
                    Rectangle topLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                    Rectangle topRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y - 75.0f), 75, 75);
                    Rectangle bottomLeft = new Rectangle((int)(ent.boundingBox.X - 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                    Rectangle bottomRight = new Rectangle((int)(ent.boundingBox.X + 75.0f), (int)(ent.boundingBox.Y + 75.0f), 75, 75);
                    Rectangle oTopLeft = new Rectangle((int)(ent.boundingBox.X - 150.0f), (int)(ent.boundingBox.Y - 150.0f), 75, 75);
                    Rectangle oTopRight = new Rectangle((int)(ent.boundingBox.X + 150.0f), (int)(ent.boundingBox.Y - 150.0f), 75, 75);
                    Rectangle oBottomLeft = new Rectangle((int)(ent.boundingBox.X - 150.0f), (int)(ent.boundingBox.Y + 150.0f), 75, 75);
                    Rectangle oBottomRight = new Rectangle((int)(ent.boundingBox.X + 150.0f), (int)(ent.boundingBox.Y + 150.0f), 75, 75);

                    if (!collision.moveOffWorld(oTopLeft) && collision.collide(oTopLeft).Count == 0 && collision.collide(topLeft).Count > 0 && ((Player)collision.collide(topLeft)[0]).enemy != ((Player)ent).enemy ||
                        !collision.moveOffWorld(oTopRight) && collision.collide(oTopRight).Count == 0 && collision.collide(topRight).Count > 0 && ((Player)collision.collide(topRight)[0]).enemy != ((Player)ent).enemy ||
                        !collision.moveOffWorld(oBottomLeft) && collision.collide(oBottomLeft).Count == 0 && collision.collide(bottomLeft).Count > 0 && ((Player)collision.collide(bottomLeft)[0]).enemy != ((Player)ent).enemy ||
                        !collision.moveOffWorld(oBottomRight) && collision.collide(oBottomRight).Count == 0 && collision.collide(bottomRight).Count > 0 && ((Player)collision.collide(bottomRight)[0]).enemy != ((Player)ent).enemy)
                    {
                        possibleMoves.Add(new Move(ent, marker));
                        ent.animate();
                    }
                }
            }
            if (possibleMoves.Count == 0) // if not jumps generated 
                generateCanMove();
        }

        /// <summary>
        /// utlity for level 1 ai
        /// </summary>
        public int utility1()
        {
            if (playerTurn && gameOver())
                return 100000;
            else if (gameOver())
                return -100000;
            int count = 0;
            for (int k = 0; k < player.Count; k++)
            {
                if (player[k].position != Vector2.Zero)
                    count--;
                if (ai[k].position != Vector2.Zero)
                    count++;
            }
            return count;
        }

        /// <summary>
        /// utlity for level 2 ai
        /// </summary>
        public int utility2()
        {
            if (playerTurn && gameOver())
                return 100000;
            else if (gameOver())
                return -100000;
            int count = 0;
            for (int k = 0; k < player.Count; k++)
            {
                switch ((int)player[k].position.Y)
                {
                    case 0:
                        break;
                    case 126:
                        break;
                    case 537:
                        count -= 5;
                        break;
                    default:
                        if (Vector2.Distance(player[k].position, new Vector2(375, 400)) < 150)
                        {
                            if (Vector2.Distance(player[k].position, new Vector2(375, 400)) < 75)
                                count += 2;
                            count += 2;
                        }
                        count -= 5;
                        break;
                }
                switch ((int)ai[k].position.Y)
                {
                    case 0:
                        break;
                    case 126:
                        count += 5;
                        break;
                    case 537:
                        break;
                    default:
                        if (Vector2.Distance(ai[k].position, new Vector2(375, 400)) < 150)
                        {
                            if (Vector2.Distance(ai[k].position, new Vector2(375, 400)) < 75)
                                count -= 2;
                            count -= 2;
                        }
                        count += 5;
                        break;
                }
            }
            return count;
        }
        /// <summary>
        /// utlity for level 3 ai
        /// </summary>
        public int utility3()
        {
            if (playerTurn && gameOver())
                return 100000;
            else if (gameOver())
                return -100000;

            int count = 0;
            for (int k = 0; k < player.Count; k++)
            {
                switch ((int)player[k].position.Y)
                {
                    case 0:
                        break;
                    case 126:
                        if (ai[k].position.X == 362)
                            count -= 2;
                        else
                            count -= 1;
                        break;
                    case 537:
                        count -= 4;
                        break;
                    default:
                        if (Vector2.Distance(player[k].position, new Vector2(375, 400)) < 150)
                        {
                            if (Vector2.Distance(player[k].position, new Vector2(375, 400)) < 75)
                                count--;
                            count--;
                        }
                        count -= 3;
                        break;
                }
                switch ((int)ai[k].position.Y)
                {
                    case 0:
                        break;
                    case 126:
                        count += 4;
                        break;
                    case 537:
                        if (ai[k].position.X == 287)
                            count += 2;
                        else
                            count += 1;
                        break;
                    default:
                        if (Vector2.Distance(ai[k].position, new Vector2(375, 400)) < 150)
                        {
                            if (Vector2.Distance(ai[k].position, new Vector2(375, 400)) < 75)
                                count++;
                            count++;
                        }
                        count += 3;
                        break;
                }
            }
            return count;
        }
        /// <summary>
        /// utlity for level 4 ai
        /// </summary>
        public int utility4()
        {
            if (playerTurn && gameOver())
                return 100000;
            else if(gameOver())
                return -100000;

            int count = 0;
            for (int k = 0; k < player.Count; k++)
            {
                switch ((int)player[k].position.Y)
                {
                    case 0:
                        break;
                    case 126:
                        count--;
                        break;
                    case 537:
                        count -= 5;
                        break;
                    default:
                        if (Vector2.Distance(player[k].position, new Vector2(375, 400)) < 150)
                        {
                            if (Vector2.Distance(player[k].position, new Vector2(375, 400)) < 75)
                                count--;
                            count--;
                        }
                        count -= 3;
                        break;
                }
                switch ((int)ai[k].position.Y)
                {
                    case 0:
                        break;
                    case 126:
                        count += 5;
                        break;
                    case 537:
                        count++;
                        break;
                    default:
                        if (Vector2.Distance(ai[k].position, new Vector2(375, 400)) < 150)
                        {
                            if (Vector2.Distance(ai[k].position, new Vector2(375, 400)) < 75)
                                count++;
                            count++;
                        }
                        count += 3;
                        break;
                }
            }
            return count;
        }

        /// check if game over
        /// </summary>
        /// <returns>If no possible move is avalible the game ends</returns>
        public bool gameOver()
        {
            return possibleMoves.Count == 0;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            foreach (Entity ent in player)
                if (ent.position != Vector2.Zero)
                    ent.Draw(spriteBatch);
            foreach (Entity ent in ai)
                if (ent.position != Vector2.Zero)
                    ent.Draw(spriteBatch);
            if (selected != -1)
            {
                foreach (Entity ent in possibleMoves[selected].markers)
                    if (ent.position != Vector2.Zero)
                        ent.Draw(spriteBatch);
            }
            spriteBatch.End();
        }
    }
}
