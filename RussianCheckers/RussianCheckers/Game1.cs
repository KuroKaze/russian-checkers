using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace RussianCheckers
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public static int winWidth = 900, winHeight = 720, environmentWidth = 450, environmentHeight = 450;
        public static Vector2 boardStart = new Vector2(100.0f, 125.0f);
        Camera camera;
        Collision collision;
        Interface inter;
        Input input;
        int gameStart = 0;
        List<Button> buttons;
        bool playerBlack;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            camera = new Camera(Vector2.Zero);
            collision = new Collision(new Rectangle((int)boardStart.X, (int)boardStart.Y, environmentWidth, environmentHeight),
                                      new Rectangle(0, 0, winWidth, winHeight),
                                      new Rectangle((int)camera.position.X, (int)camera.position.Y, winWidth, winHeight));
            input = new Input(this);
            input.initGraphics(graphics, false, winWidth, winHeight);
            buttons = new List<Button>();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            inter = new Environment(Content.Load<Texture2D>("Board"), new Rectangle(0, 0, environmentWidth, environmentHeight), input, camera, collision, boardStart);
            // TODO: use this.Content to load your game content here
            buttons.Clear();
            buttons.Add(new Button(Content.Load<Texture2D>("NewGameButton"), new Vector2(winWidth - 200, 675), input));
            buttons.Add(new Button(Content.Load<Texture2D>("Black"), new Vector2(winWidth / 2 - 200, 300), input));
            buttons.Add(new Button(Content.Load<Texture2D>("Red"), new Vector2(winWidth / 2 + 100, 300), input));
            buttons.Add(new Button(Content.Load<Texture2D>("AILVL1"), new Vector2(winWidth / 2 - 50, 300), input));
            buttons.Add(new Button(Content.Load<Texture2D>("AILVL2"), new Vector2(winWidth / 2 - 50, 400), input));
            buttons.Add(new Button(Content.Load<Texture2D>("AILVL3"), new Vector2(winWidth / 2 - 50, 500), input));
            buttons.Add(new Button(Content.Load<Texture2D>("AILVL4"), new Vector2(winWidth / 2 - 50, 600), input));
            //collision.add(markers);
            //inter.addComponent(Content.Load<Texture2D>("Board"), boardStart, Content.Load<SpriteFont>("Font/Arial"));

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // TODO: Add your update logic here
            switch (gameStart)
            {
                case 0:
                    for (int i = 1; i < 3; i++)
                    {
                        if (buttons[i].clicked())
                        {
                            switch (i)
                            {
                                case 1:
                                    playerBlack = true;
                                    break;
                                case 2:
                                    playerBlack = false;
                                    break;
                            }
                            gameStart++;
                        }
                    }
                    break;
                case 1:
                    for (int i = 3; i < buttons.Count; i++)
                    {
                        if (buttons[i].clicked())
                        {
                            ((Environment)inter).LoadContent(Content, playerBlack);
                            switch (i)
                            {
                                case 3:
                                    ((Environment)inter).aiLevel = 1;
                                    break;
                                case 4:
                                    ((Environment)inter).aiLevel = 2;
                                    break;
                                case 5:
                                    ((Environment)inter).aiLevel = 3;
                                    break;
                                case 6:
                                    ((Environment)inter).aiLevel = 4;
                                    break;
                            }
                            buttons[i].newPosition(new Vector2(winWidth - 100, 675));
                            inter.addButton(buttons[i]);
                            inter.addComponent(Content.Load<Texture2D>("RightPanel"), new Vector2(winWidth - 225, 0), Content.Load<SpriteFont>("Font/Arial"));
                            inter.updateText(1, "Controls\n" +
                                                " >> Movable pieces are\n" +
                                                "    highlighted in BLUE\n" +
                                                " >> Click to select a piece\n" +
                                                " >> Selected piece show\n" +
                                                "    possible moves in YELLOW\n" +
                                                " >> Click to move selected\n" +
                                                "    piece");
                            gameStart = 2;
                        }
                    }
                    break;
                case 2:
                    ((Environment)inter).Update(gameTime);
                    if (buttons[0].clicked())
                    {
                        gameStart = 0;
                        this.Initialize();
                        this.LoadContent();
                    }
                    break;
            }
            
            input.Update();
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Gray);
            switch(gameStart)
            {
                case 0:
                    spriteBatch.Begin();
                    string startMenu = "Choose a color, Black goes first";
                    string disclaimer = "(The game will hold for ~1 min for computer to move)";
                    spriteBatch.DrawString(Content.Load<SpriteFont>("Font\\Arial"), startMenu, new Vector2(winWidth / 2 - Content.Load<SpriteFont>("Font\\Arial").MeasureString(startMenu).X/2, 200), Color.Black);
                    spriteBatch.DrawString(Content.Load<SpriteFont>("Font\\Arial"), disclaimer, new Vector2(winWidth / 2 - Content.Load<SpriteFont>("Font\\Arial").MeasureString(disclaimer).X / 2, 225), Color.Black);
                    for (int i = 1; i < 3; i++)
                        buttons[i].Draw(spriteBatch);
                    spriteBatch.End();
                    break;
                case 1:
                    spriteBatch.Begin();
                    string aiMenu = "Choose a level for AI ";
                    spriteBatch.DrawString(Content.Load<SpriteFont>("Font\\Arial"), aiMenu, new Vector2(winWidth / 2 - Content.Load<SpriteFont>("Font\\Arial").MeasureString(aiMenu).X / 2, 200), Color.Black);
                    for (int i = 3; i < buttons.Count; i++)
                        buttons[i].Draw(spriteBatch);
                    spriteBatch.End();
                    break;
                case 2:
                    inter.DrawBG(spriteBatch);
                    inter.DrawGUI(spriteBatch);
                    ((Environment)inter).Draw(spriteBatch);
                    spriteBatch.Begin();
                    buttons[0].Draw(spriteBatch);
                    spriteBatch.End();
                    break;
            }
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
