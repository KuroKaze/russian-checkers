﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace RussianCheckers
{
    class Interface
    {
        protected List<Texture2D> backgrounds;
        protected List<Vector2> positions;
        protected List<SpriteFont> fonts;
        protected List<Entity> entities;
        protected List<String> text;
        public List<Button> buttons;
        protected Rectangle enviBox;
        protected Input input;
        protected Camera camera;
        protected Collision collision;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="environment">Background texture</param>
        /// <param name="environmentBox">Rectangle texture source</param>
        /// <param name="inp">Input object</param>
        /// <param name="cam">Camera object</param>
        /// <param name="colli">Collision object</param>
        /// <param name="loc">Position of environment</param>
        public Interface(Texture2D environment, Rectangle environmentBox, Input inp, Camera cam, Collision colli, Vector2 loc)
        {
            backgrounds = new List<Texture2D>();
            backgrounds.Add(environment);
            positions = new List<Vector2>();
            positions.Add(loc);
            fonts = new List<SpriteFont>();
            fonts.Add(null);
            entities = new List<Entity>();
            text = new List<String>();
            text.Add(null);
            buttons = new List<Button>();

            enviBox = environmentBox;

            input = inp;
            camera = cam;
            collision = colli;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="inter">Interface object to be copied</param>
        public Interface(Interface inter)
        {
            backgrounds = new List<Texture2D>();
            //foreach (Texture2D texture in inter.backgrounds)
            //    backgrounds.Add(texture);
            positions = new List<Vector2>();
            //foreach (Vector2 vector in inter.positions)
            //positions.Add(new Vector2(vector);
            fonts = new List<SpriteFont>();
            fonts = inter.fonts;
            entities = new List<Entity>();
            entities = inter.entities;
            text = new List<string>();
            text = inter.text;
            buttons = new List<Button>();
            buttons = inter.buttons; ;
            enviBox = inter.enviBox;
            input = inter.input;
            camera = inter.camera;
            collision = new Collision(inter.collision);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="inp">Input object</param>
        /// <param name="cam">Camera object</param>
        /// <param name="colli">Collision object</param>
        public Interface(Input inp, Camera cam, Collision colli)
        {
            backgrounds = new List<Texture2D>();
            backgrounds.Add(null);
            positions = new List<Vector2>();
            positions.Add(Vector2.Zero);
            fonts = new List<SpriteFont>();
            fonts.Add(null);
            entities = new List<Entity>();
            text = new List<String>();
            text.Add(null);
            buttons = new List<Button>();

            enviBox = new Rectangle(0, 0, 0, 0);

            input = inp;
            camera = cam;
            collision = colli;
        }

        /// <summary>
        /// Add component to HUD
        /// </summary>
        /// <param name="texture">Background texture for new component</param>
        /// <param name="loc">Location of new component</param>
        /// <param name="font">Font being used for new component</param>
        public void addComponent(Texture2D texture, Vector2 loc, SpriteFont font)
        {
            backgrounds.Add(texture);
            positions.Add(loc);
            fonts.Add(font);
            text.Add("");
        }

        /// <summary>
        /// Add entity to HUD
        /// </summary>
        /// <param name="ent">Entity to be added</param>
        public void addEntityToComponent(Entity ent)
        {
            entities.Add(ent);
        }

        /// <summary>
        /// Add button to HUD
        /// </summary>
        /// <param name="sprite">Button texture</param>
        /// <param name="loc">Button location</param>
        public void addButton(Texture2D sprite, Vector2 loc)
        {
            buttons.Add(new Button(sprite, loc, input));
        }
        
        /// <summary>
        /// Add button to HUD
        /// </summary>
        /// <param name="button">Button tobe added</param>
        public void addButton(Button button)
        {
            buttons.Add(button);
        }

        /// <summary>
        /// Remove entity from HUD
        /// </summary>
        /// <param name="ent">Entity to be removed</param>
        public void removeEnity(Entity ent)
        {
            entities.Remove(ent);
        }

        /// <summary>
        /// Remove all entities from HUD
        /// </summary>
        public void removeEntities()
        {
            entities.Clear();
        }

        /// <summary>
        /// Change text displayed in HUD
        /// </summary>
        /// <param name="compNum">Component no. of HUD</param>
        /// <param name="str">New text</param>
        public void updateText(int compNum, String str)
        {
            text[compNum] = str;
        }

        /// <summary>
        /// Draw Entities on HUD
        /// </summary>
        /// <param name="spriteBatch">Enables entities to be drawn</param>
        public virtual void drawEntities(SpriteBatch spriteBatch)
        {

        }

        /// <summary>
        /// Virtual Update function for interface
        /// Call input.Update(); in last line of main update function!!!!
        /// </summary>
        public virtual void Update()
        {
            if (input.keyPressed(Keys.Escape))
                input.Exit();
        }

        /// <summary>
        /// Draw HUD
        /// </summary>
        /// <param name="spriteBatch">Enables HUD to be drawn</param>
        public void DrawGUI(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            for (int i = 1; i < backgrounds.Count; i++)
            {
                spriteBatch.Draw(backgrounds[i], positions[i], Color.White);
                spriteBatch.DrawString(fonts[i], text[i], positions[i] + new Vector2(5.0f, 5.0f), Color.White);
            }

            foreach (Button but in buttons)
            {
                but.Draw(spriteBatch);
            }
            drawEntities(spriteBatch);
            spriteBatch.End();
        }

        /// <summary>
        /// Draw background
        /// </summary>
        /// <param name="spriteBatch">Enables background image to be drawn</param>
        public void DrawBG(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, SamplerState.LinearWrap, null, null, null, camera.getTransformation());
            spriteBatch.Draw(backgrounds[0], positions[0], enviBox, Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0);
            spriteBatch.End();
        }
    }

}