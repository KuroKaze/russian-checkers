﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RussianCheckers
{
    class Player : Entity
    {
        public bool enemy;

        public Player(Texture2D texture, int numRows, int numColumns, int startFrame, int endFrame, Vector2 loc, bool eny)
            : base(texture, numRows, numColumns, startFrame, endFrame, loc)
        {
            enemy = eny;
        }
        public Player(Player ent)
            : base(ent)
        {
            enemy = ent.enemy;
        }
    }
}
